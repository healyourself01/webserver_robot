import json, jsonmerge, requests
from pprint import pprint

couch_url = "http://139.59.68.206:5984/new_database/com_mindgame_image"

schema = {
           "properties": {
                "bar": {
                     "mergeStrategy": "append"
                 }
             }
         }

data = requests.get(couch_url).content
data = json.loads(requests.get(couch_url).content)

addition = {
    "ad_token":"TEST"
}
result = jsonmerge.merge(data, addition)
# pprint (result, width=40)

resp = requests.put(couch_url ,json.dumps(result) )
pprint(resp.content)

data = requests.get(couch_url).content
data = json.loads(data)
pprint (data, width=40)