# -------- This file is application config and it is trackd. It is common to both PROD and TEST
import os, time
from datetime import datetime
import pymongo
from pymongo import MongoClient
from Andromeda_Config import MONGODB_URL
client = MongoClient(MONGODB_URL)
print ("Connected to MONGO at:", MONGODB_URL)


OUTPUT_DIR = "C:/Users/mindgame/Google Drive/APK_Downloads"
FIREFOX_PROFILE = "C:/Users/Ajay/AppData/Roaming/Mozilla/Firefox/Profiles/rh35ia2y.default"
DATABASE_NAME = "andromeda"
db = client[DATABASE_NAME]
IMAGE_PREFIX_DIR = "static/production_robot_apps"

# BUCKET_BASE = "https://storage.googleapis.com/robot_01/"

headers = {"content-type": "application/json", "Authorization": "<auth-key>"}
# -----------DIR NAMES-----------------
ORIGINAL_DIR_STRING = "01_original_images"
VALID_DIR_STRING = "02_valid_images"
WATERMARK_DIR_STRING = "03_watermark_images"
TEXT_DIR_STRING = "03_text_images"
FACE_DIR_STRING = "03_face_images"
SET_IMAGE_FOLDER = "08_set_images"
PUBLISHED_DIR = "Published"
IN_PROCESS_DIR = "InProcess"
ORIGINAL_DIR = "01_original_images"
QUEUE_DIR = "Queue"
IN_PROCES_DIR ="InProcess"
LOG_DIR ="10_Logs"
# ----------------------------
APP_NAME = ""
SET1_STR = "set_1"
SET2_STR = "set_2"
SET3_STR = "set_3"
SET4_STR = "set_4"
SET5_STR = "set_5"
SET6_STR = "set_6"
SET7_STR = "set_7"
SET8_STR = "set_8"
SET9_STR = "set_9"
SET10_STR = "set_10"

IN_APP_GRAPHICS = "05_In_App_Graphics"
SCREEN_SHOTS = "06_ScreenShots"
APK_KEY = "07_APK_Key"
IMAGE_FILE_EXTENSIONS = ('.jpg', '.jpeg', ".png")
# IMAGE_FILE_EXTENSIONS = ('.webp')
CURR_DIR = str(os.path.dirname(os.path.abspath(__file__)))
print ("Current Wokring Dir:", CURR_DIR)
ROBOT_DIR = os.path.join(CURR_DIR, "static//production_robot_apps")

# -------------------MONGO DB COLLECTION NAMES--------------
# The kind for the keywords
collection_name_keywords = 'keywords'
collection_name_developerlist = 'developers'
FIREBASE_DDEVELOPER_LIST = "https://robot01-a668c.firebaseio.com/playstore/developers.json"

# -----------------CURRENT DATE---------------
CURRENT_DATE = datetime.now().strftime("%Y-%m-%d")
# ----------------Days Delta-----------------------
DaysDelta = 180

# ------------------ANALYTICS CONSTANTS-------------------------
# query string base url
QUERY_BASE_HEAD = 'https://play.google.com/store/search?q='
QUERY_BASE_TAIL = '&c=apps'
# DEV string base url
DEVELOPER_BASE_URL_STRING = 'https://play.google.com/store/apps/developer?id='
DEVELOPER_BASE_URL_INT = 'https://play.google.com/store/apps/dev?id='


# ----------REQUESTS HEADERS--------------
HEADERS_SCRAPER={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'}
REQUESTS_TIME_DELTA = 0.3

