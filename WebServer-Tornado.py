# pip install tornado
import tornado.ioloop
import tornado.web
import os, json
import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid
import JSONCreator
import requests, time
from AndromedaPy import logger_andromeda,get_subdirectories
from Andromeda_Config import PORT, HOST_IP, FIREBASE_DB, COUCH_DB, BUCKET_BASE

from tornado.options import define, options
from ImgDownloader import download_images_bing, download_urllist
from AndromedaPy import absolute_url
import shutil
# Application constants
from APP_CONFIG import *
import Scraper_Functions as SF
import AndromedaPy as AP
import DataDescriptions as DD



# ------------------------------------------------------------------------------------------
def create_dir(app_dir, dir_temp):
    path = os.path.join(ROBOT_DIR, QUEUE_DIR, app_dir, dir_temp)
    if not os.path.isdir(path):
        os.mkdir(path)
"""
------------------------------------------------------------------
is_image(filename)
checks whether the file is image and returns True or False
------------------------------------------------------------------
"""


def is_image(filename):
    if os.path.splitext(filename)[1].lower() in IMAGE_FILE_EXTENSIONS:
        return True
    else:
        return False
"""
------------------------------------------------------------------
move_item(item, app_dir, opt_dir)
Moves the item to corresponding Dir
------------------------------------------------------------------
"""

def move_item(item, app_dir, opt_dir):
    # get the image name from the
    item = str(item).split("\\")[-1]
    SOURCE_DIR = os.path.join(ROBOT_DIR,QUEUE_DIR,app_dir,ORIGINAL_DIR_STRING,item)
    DEST_DIR = os.path.join(ROBOT_DIR,QUEUE_DIR,app_dir,opt_dir,item)
    shutil.move(SOURCE_DIR, DEST_DIR)
    print "Moved item {0} to Dir:{1}".format(item, opt_dir)

def getImageURLs(app_dir, image_limit):
    count=0
    dirimglist = os.listdir(os.path.join(ROBOT_DIR, QUEUE_DIR, app_dir,ORIGINAL_DIR))
    imglist = []
    for item in dirimglist:
        if is_image(item):
            imageitem= ""
            imageitem = os.path.join(IMAGE_PREFIX_DIR,QUEUE_DIR,app_dir,ORIGINAL_DIR, item)
            imglist.append(imageitem)
            count += 1
        if count>=image_limit:
            break
    print imglist
    return imglist


def fnRename_images(app_dir, stage_dir):
    SOURCE_DIR = os.path.join(ROBOT_DIR, stage_dir, app_dir, ORIGINAL_DIR_STRING)
    app_name = str(app_dir).lower()
    count = 0
    for image in os.listdir(SOURCE_DIR):
        if is_image(image):
            if image.startswith("Img_"): #If the dir is lready renamed, break the loop
                break
            new_name = "Img_" +str(count) +"_" + app_name + "." +str(image).split(".")[-1]
            # os.rename(image, new_name)
            OLD_NAME = os.path.join(SOURCE_DIR,image)
            DEST_DIR = os.path.join(ROBOT_DIR, stage_dir, app_dir, ORIGINAL_DIR_STRING, new_name)
            shutil.move(OLD_NAME, DEST_DIR)
            count += 1
    return "Images Renamed Successfully"


# --------------------------------Path Handlers-----------------------------------------------------
class Application(tornado.web.Application):

    def __init__(self):

        handlers = [

            (r"/", MainHandler),
            (r"/Queue", QueueHandler),
            (r"/Queue/App", QueueAppHandler),
            (r"/Queue/Crt_App", QueueCrtAppHandler),
            (r"/Queue/Validate", ValidationHandler),
            (r"/InProgress", InProgressHandler),
            (r"/InProgress/Process", InProgressAppHandler),
            (r"/InProgress/AppData", InProgressAppDataEntry),
            (r"/InProgress/AppValidation", InProgressAppValidationHandler),
        #--------------Published Apps-----------------------------
            (r"/Published", PublishedHandler),
            (r"/published_websocket", PublishedAppWebSocket),
            (r"/Published/Titles", PublishedTitlesHandler),
        #--------------Image Upload-----------------------------
            (r"/ImgUploader", ImgUploadHandler),
        #--------------Image Upload-----------------------------
            (r"/Img", ImgHandler),
            (r"/img_websocket", ImgWebSocket),
            (r"/Img/Bing", ImgBingHandler),
            (r"/Img/URLList", ImgURLListHandler),
            (r"/image_validation_ws", ImageValidation_WS),
            (r"/processapp", ProcessAppWebSocket),
            (r"/queue_websocket", QueueWebSocket),
        # ------------Analytics----------------------
            (r"/Analytics", AnlHomeHandler),
            (r"/anl_websocket", AnlWebSocket),
            (r"/Analytics/InputData", AnlInputDataHandler),

        ]

        settings = dict(

            blog_title=u"Tornado Blog",

            template_path=os.path.join(os.path.dirname(__file__), "templates"),

            static_path=os.path.join(os.path.dirname(__file__), "static"),
            # ui_modules={"Entry": EntryModule},

            xsrf_cookies=False,

            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",

            login_url="/auth/login",

            debug=True,

        )

        super(Application, self).__init__(handlers, **settings)


class ImageValidation_WS(tornado.websocket.WebSocketHandler):
    def open(self):
        print("WebSocket opened")

    def on_message(self, message):
        print "Web Socket Message Received."
        print message
        image_data = open("images_data.json", "w")
        image_data.write(json.dumps(json.loads(message), indent=4))
        image_data.close()
        # Loop through the JSON and move the images accrodingly.
        validation_json = json.loads(message)
        # Get the origin details first
        if validation_json[0]['origin'] == "validation":
            app_name = validation_json[0]['app_name']
            # Delete the first item as it has the origin info
            del validation_json[0]
        #     Loop through teh JSON
            for item in validation_json:
                if item['option'] == "valid":
                    move_item(item=item['item'], app_dir=app_name, opt_dir=VALID_DIR_STRING)
                elif item['option'] == "watermark":
                    move_item(item=item['item'], app_dir=app_name, opt_dir=WATERMARK_DIR_STRING)
                elif item['option'] == "text":
                    move_item(item=item['item'], app_dir=app_name, opt_dir=TEXT_DIR_STRING)
                elif item['option'] == "face":
                    move_item(item=item['item'], app_dir=app_name, opt_dir=FACE_DIR_STRING)
        # Refresh the page to load next set of images
            self.write_message("success")
    # -----------------------------PROCESS-APP--------------------------------------------------
        # Get the origin details first
        if validation_json['origin'] == "process-app":
            app_name = validation_json['app_name']
            # Delete the first item as it has the origin info
            print "Creating JSON for App:", app_name

    def on_close(self):
        print("WebSocket closed")

class ProcessAppWebSocket(tornado.websocket.WebSocketHandler):
    def open(self):
        print("WebSocket opened")

    def on_message(self, message):
        print "Web Socket Message Received."
        print message
        app_json = json.loads(message)
        # Get the origin details first
    # -----------------------------PROCESS-APP--------------------------------------------------
        # Get the origin details first
        if app_json['origin'] == "process-app" and app_json['action'] == "CrtJSON":
            app_name = app_json['app_name']
            stage_dir= app_json['stage_dir']
            mode= app_json['mode']
            # Delete the first item as it has the origin info
            print "Creating JSON for App:", app_name
            result = str(JSONCreator.create_json(stage_dir,app_name,mode))
            self.write_message(result)
    # --------------------------APP-DATA-ENTRY-------------------------------------------------------------
        # --------------------------2.SET TEST MODE--------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnSetTestMode":
            headers = {"content-type": "application/json", "Authorization": "<auth-key>"}
            app_name = str(app_json['app_name']).strip()
            stage_dir = app_json['stage_dir']
            mode = app_json['mode']

            print "Setting Test Mode to:", app_name
            app_json['self'] = self
            print app_json

            JSONCreator.set_test_mode(app_json)
        # --------------------------2.1.WEBP CONVERSION--------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnConvertToWEBP":
            headers = {"content-type": "application/json", "Authorization": "<auth-key>"}
            app_name = str(app_json['app_name']).strip()
            print "Converting images to WEBP:", app_name
            app_json['self'] = self
            print app_json

            JSONCreator.btnConvertToWEBP(app_json)

        # --------------------------3.Add base JSON--------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnAddBaseJSON":
            headers = {"content-type": "application/json", "Authorization": "<auth-key>"}
            app_name = str(app_json['app_name']).strip()
            print "Setting Test Mode to:", app_name
            app_json['self'] = self
            print app_json

            JSONCreator.add_base_json(app_json)

        # --------------------------4.Make Sets--------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnMakeSets":
            headers = {"content-type": "application/json", "Authorization": "<auth-key>"}
            app_name = str(app_json['app_name']).strip()
            print "Setting Test Mode to:", app_name
            app_json['self'] = self
            print app_json

            JSONCreator.make_sets(app_json)

        # --------------------------4.Make Sets Title Images--------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnMakeSetTitleImages":
            headers = {"content-type": "application/json", "Authorization": "<auth-key>"}
            app_name = str(app_json['app_name']).strip()
            app_json['self'] = self
            print app_json

            JSONCreator.set_title_images(app_json)
        # --------------------------MINIO UPLOAD-------------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnUploadtoMinio":
            app_name = app_json['app_name']
            print "Uploading to MINIO Cloud App:", app_name
            print app_json
            app_json['self'] = self
            JSONCreator.btnUploadtoMinio(app_json)

        # --------------------------MINIO DELETE-------------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnDelFromMinio":
            app_name = app_json['app_name']
            print "Uploading to MINIO Cloud App:", app_name
            print app_json
            app_json['self'] = self
            JSONCreator.btnDelFromMinio(app_json)

        # --------------------------GCLOUD UPLOAD-------------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnUploadtoGcloud":
            app_name = app_json['app_name']
            print "Uploading to Google Cloud App:", app_name
            print app_json
            app_json['self'] = self
            JSONCreator.upload_gcloud(app_json)

        # --------------------------GCLOUD DELETE-------------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnDelFromGcloud":
            app_name = app_json['app_name']
            print "Uploading to Google Cloud App:", app_name
            print app_json
            app_json['self'] = self
            JSONCreator.delete_gcloud(app_json)

        # --------------------------TEST JSON-------------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnTestJSON":
            app_name = app_json['app_name']
            print "btnTestJSON:", app_name
            print app_json
            app_json['self'] = self
            JSONCreator.test_json(app_json)

        # --------------------------PRE PUBLISH SETUP-------------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnPrePublishSetup":
            app_name = app_json['app_name']
            print "btnPrePublishSetup:", app_name
            print app_json
            app_json['self'] = self
            JSONCreator.pre_publish_setup(app_json)

        # --------------------------Post PUBLISH SETUP-------------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnPostPublishSetup":
            app_name = app_json['app_name']
            print "btnPrePublishSetup:", app_name
            print app_json
            app_json['self'] = self
            JSONCreator.post_publish_setup(app_json)

        # --------------------------MOVE TO PUBLISH-------------------------------------------------------------
        if app_json['origin'] == "process-app" and app_json['action'] == "btnMoveToPublished":
            app_name = app_json['app_name']
            print "btnMoveToPublished:", app_name
            print app_json
            app_json['self'] = self
            message = JSONCreator.move_app(app_json['app_name'],IN_PROCESS_DIR, PUBLISHED_DIR)
            self.write_message(message)

        # ----------------------------------App Data Ma intaining-------------------------------------------------
        if app_json['origin'] == "app-data" and app_json['action'] == "btnSubmit":
            headers = {"content-type": "application/json", "Authorization": "<auth-key>"}
            app_name = str(app_json['app_name']).strip()
            stage_dir = app_json['stage_dir']
            print "Updating the app data for:", app_name
            print app_json
            print "Create/Update Entry in Database"

            app_data = dict()
            app_data['app_name'] = app_json['app_name']
            # ----------------------------Create admob_detail from ad units---------------------------------
            app_data["admob_details"] = {
                "banner": [app_json['banner_id']],
                "interstitial": [
                    app_json['inter_id1'],
                    app_json['inter_id2'],
                    app_json['inter_id3'],
                    app_json['inter_id4'],
                    app_json['inter_id1'],
                    app_json['inter_id2'],
                    app_json['inter_id4'],
                    app_json['inter_id3'],
                    app_json['inter_id2'],
                    app_json['inter_id1']
                ]
            }
            #Add socket to APP_JSON
            app_json['self'] = self
            app_json['app_name'] = app_json['package']

            # ---- 3.Write the App JSON with Package name as Key finally------------
            # ---- 2.Update the package-->appname ------------
            package_name_modified = app_json['package']

            FIREBASE_APP_DB = FIREBASE_DB + "app_database" + "/" +  package_name_modified + ".json"
            result = requests.patch(FIREBASE_APP_DB, data=json.dumps(app_json['app_name']), headers=headers)

            # ---- 1.Update the aapname-->packagename------------
            FIREBASE_APP_DB = FIREBASE_DB + "app_database" + "/" + app_json['app_name'] + ".json"
            result = requests.patch(FIREBASE_APP_DB, data=json.dumps(package_name_modified), headers=headers)

            JSONCreator.firebase_put(app_json, data=app_data)

    def on_close(self):
        print("WebSocket closed")



# Queue Processing websocket

class QueueWebSocket(tornado.websocket.WebSocketHandler):
    def open(self):
        print("WebSocket opened")

    def on_message(self, message):
        print "Web Socket Message Received."
        print message
        app_json = json.loads(message)
        # Get the origin details first
    # -----------------------------QUEUE-APP--------------------------------------------------
        # --------Step1:Create APP--------------------------------------------------
        if app_json['origin'] == "queue-app" and app_json['action'] == "btnCreateApp":
            print "btnCreateApp:", app_json['package']
            app_json['self'] = self
            result = JSONCreator.create_app(app_json)

        # --------Step5:Move App to In Progress--------------------------------------------------
        if app_json['origin'] == "queue-app" and app_json['action'] == "btnMovToInProgress":
            app_name = app_json['app_name']
            stage_dir= app_json['stage_dir']
            mode= app_json['mode']
            # Move the App Dir to InProcess
            print "Moving {0} to In Progress.", app_name
            result = JSONCreator.move_app(app_name, stage_dir, IN_PROCESS_DIR)
            self.write_message("App moved to InProgress Successfully.")
        # --------Step1:Create App Directories--------------------------------------------------
        if app_json['origin'] == "queue-app" and app_json['action'] == "btnCrtAppDir":
            app_name = app_json['app_name']
            stage_dir= app_json['stage_dir']
            mode= app_json['mode']
            # Move the App Dir to InProcess
            print "Step2:Rename Images:", app_name
            app_json['self'] =self
            result = JSONCreator.create_app_dir(app_json)
            # self.write_message(result)

        # --------Step2.1:Delete All Images--------------------------------------------------
        if app_json['origin'] == "queue-app" and app_json['action'] == "btnDelAllImages":
            app_name = app_json['app_name']
            stage_dir= app_json['stage_dir']
            mode= app_json['mode']
            TARGET_DIR = os.path.join(ROBOT_DIR,QUEUE_DIR, app_name, ORIGINAL_DIR_STRING)
            print "Step2:Delete All Images:", app_name
            result = JSONCreator.clear_dir(TARGET_DIR)
            self.write_message(result)

        # --------Step2:Rename Images--------------------------------------------------
        if app_json['origin'] == "queue-app" and app_json['action'] == "btnRenImages":
            app_name = app_json['app_name']
            stage_dir= app_json['stage_dir']
            mode= app_json['mode']
            print "Step2:Rename Images:", app_name
            result = fnRename_images(app_name, stage_dir)
            self.write_message(result)

        # --------Step3:Move All Images to Valid Directory--------------------------------------------------
        if app_json['origin'] == "queue-app" and app_json['action'] == "btnMoveAllToValid":
            app_name = app_json['app_name']
            stage_dir= app_json['stage_dir']
            mode= app_json['mode']
            print "Step3:Move All Images to Valid Directory:", app_name
            result = JSONCreator.move_all_images(app_name, stage_dir, ORIGINAL_DIR_STRING, VALID_DIR_STRING)
            self.write_message("All Images Moved to Valid dir.")

    def on_close(self):
        print("WebSocket closed")



class MainHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the count in each Direcotry
        item_count = {}
        item_count['Queue'] = len(os.listdir(os.path.join(ROBOT_DIR,QUEUE_DIR)))
        item_count['InProcess'] = len(os.listdir(os.path.join(ROBOT_DIR,IN_PROCESS_DIR)))
        item_count['Published'] = len(os.listdir(os.path.join(ROBOT_DIR,PUBLISHED_DIR)))
        self.render("home.html", item_count=item_count)

class InProgressHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in InProgress and pass to home page
        apps = get_subdirectories(IN_PROCESS_DIR)
        self.render("inprogress-home.html", apps=apps)

class InProgressAppHandler(tornado.web.RequestHandler):
    def get(self):
        # Validate the App data and generate the report
        print self.request.arguments
        app_name = self.request.arguments['app_name'][0]
        self.render("process-app.html", app_name=app_name)

class InProgressAppValidationHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in QUEUE and pass to home page
        print self.request.arguments
        app_name = self.request.arguments['app_name'][0]

        error_messages = JSONCreator.validate_app_data(IN_PROCESS_DIR, app_name)
        print error_messages
        self.render("validatate-appdata.html", error_messages=error_messages, app_name=app_name)

# -----------------------------APP DATA ENTRY ------------------------------------------
class InProgressAppDataEntry(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in QUEUE and pass to home page
        package_names = {"NoPackages":""}

        print self.request.arguments
        app_name = str(self.request.arguments['app_name'][0]).strip()
        app_database = requests.get(FIREBASE_DB+"app_database.json").content
        app_database = json.loads(str(app_database).decode('utf-8'))

        if app_database is None:
            app_database = {}

        app_data_dummy = {

            "origin":"",
            "action": "",
            "stage_dir": "",
            "mode":"",
            "app_name":"",
            "package": "",
            "banner_id": "",
            "inter_id1": "",
            "inter_id2": "",
            "inter_id3": "",
            "inter_id4": "",
            "desc_short": "",
            "desc_detailed": ""
        }
        firebase_app_data = app_data_dummy
        # ---- 3.Write the App JSON with Package name as Key finally------------
        result = requests.get(FIREBASE_DB+app_name+".json")
        if result.content != "null":
            firebase_app_data = json.loads(result.content)
        # If "app_name" not defined in result, load dummy
        if ("app_name" not in firebase_app_data) or ("banner_id" not in firebase_app_data):
            firebase_app_data = app_data_dummy

        print "App Data from Firebase:",  str(firebase_app_data)
        self.render("app-data.html", app_name=app_name, app_database_keys=app_database.keys(),
                    app_database=app_database, firebase_app_data = firebase_app_data)


# -----------------------------APP DATA ENTRY ------------------------------------------
# -----------------------------IMAGE DOWNLOADER ------BEG------------------------------------


class ImgHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in QUEUE and pass to home page
        self.render("img-home.html")

class ImgBingHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in QUEUE and pass to home page
        self.render("img-bing.html")

class ImgURLListHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in QUEUE and pass to home page
        self.render("img-urllist.html")

class ImgWebSocket(tornado.websocket.WebSocketHandler):
    def open(self):
        print("ImgWebSocket WebSocket opened")

    def on_message(self, message):
        print "Web Socket Message Received."
        print message
        app_json = json.loads(message)
        # Get the origin details first
        # -----------------------------Img-Bing--------------------------------------------------
        if app_json['origin'] == "Img/Bing" and app_json['action'] == "btnSubmitBingImgSearch":
            app_name = app_json['app_name']
            stage_dir = app_json['stage_dir']
            # Download the Images with bing search
            outdir = str(app_json['OutDir']).replace("\\", "//")
            download_images_bing(outdir=app_json['OutDir'], word=app_json['SrchStr'],
                                 minHight=100, minWidth=50, img_type="photo", img_layout="tall",
                                 maxImages= int(app_json['NoOfImgs']))
            self.write_message("Images downloaded.")

        # -----------------------------Img-Bing--------------------------------------------------
        if app_json['origin'] == "Img/URLList" and app_json['action'] == "btnSubmitURLList":
            download_urllist(app_json)
            self.write_message("Images downloaded.")


# -----------------------------IMAGE DOWNLOADER -END-----------------------------------------

# -----------------------------PLAYSTORE ANALYSTICS ------BEG------------------------------------


class AnlHomeHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in QUEUE and pass to home page
        self.render("anl-home.html")

class AnlInputDataHandler(tornado.web.RequestHandler):
    def get(self):
        PLDEVLST_DATA = AP.mongo_read(collection= DD.COLLECTION_00_PLDEVLST, qstring=None)
        PLKWDLST_DATA = AP.mongo_read(collection= DD.COLLECTION_00_PLKWDLST, qstring=None)
        # Get the list of directories in QUEUE and pass to home page
        self.render("anl-input-data.html", PLDEVLST=PLDEVLST_DATA, PLKWDLST=PLKWDLST_DATA)

class AnlWebSocket(tornado.websocket.WebSocketHandler):
    def open(self):
        print("AnlWebSocket WebSocket opened")

    def on_message(self, message):
        print "AnlWebSocket Message Received."
        print message
        dev_data = json.loads(message)
        # -----------------------------ANL-INPUT-DATA----ADD--DEVELOPER--------------------------------------------
        if dev_data['origin'] == "anl-input-data" and dev_data['action'] == "btnCreateDev":
            # Add the developers to DB
            message = SF.add_developer(dev_data)
            self.write_message(message)
        # -----------------------------ANL-INPUT-DATA----ADD--KEYWORD--------------------------------------------
        if dev_data['origin'] == "anl-input-data" and dev_data['action'] == "btnCreateKwd":
            # Add the keyword to DB
            message = SF.add_keyword(dev_data)
            self.write_message(message)
# -----------------------------PLAYSTORE ANALYSTICS -END-----------------------------------------
# -----------------------------PUBLISHED APP HANDLER---------------------------------------------
class PublishedHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in PUBLISHED and pass to home page
        apps = get_subdirectories(PUBLISHED_DIR)
        self.render("published-home.html", apps=apps)

class PublishedTitlesHandler(tornado.web.RequestHandler):
    def get(self):
        print self.request.arguments
        app_name = str(self.request.arguments['app_name'][0]).strip()
        app_data_raw = requests.get(FIREBASE_DB+ app_name + ".json").content
        app_data = json.loads(str(app_data_raw).decode('utf-8'))
        app_titles = []

        app_data_dummy = ['', '', '', '', '', '', '', '', '', '', ]

        if app_data is None:
            app_titles = app_data_dummy
        else:
            prod_images = app_data['prod_images']
            for item in prod_images:
                app_titles.append(item['title'])


        self.render("published-app-titles.html", app_titles=app_titles,
                    app_name = app_name, firebase_app_data = app_data)
# -------------PUBLISHED APP WEBSOCKET---------------------------------------------------------------
class PublishedAppWebSocket(tornado.websocket.WebSocketHandler):
    def open(self):
        print("PublishedAppWebSocket WebSocket opened")

    def on_message(self, message):
        print "Web Socket Message Received."
        print message
        app_json = json.loads(message)
        # Get the origin details first
        # -----------------------------PROCESS-APP--------------------------------------------------
        # Get the origin details first
        if app_json['origin'] == "published-app-titles" and app_json['action'] == "btnSubmit":
            package = app_json['package']
            stage_dir = app_json['stage_dir']
            # Delete the first item as it has the origin info
            print "Updating titles for App:", package
            app_data_raw = requests.get(FIREBASE_DB + package + ".json").content
            app_data = json.loads(str(app_data_raw).decode('utf-8'))
            app_titles = app_json['app_titles']

            if app_data is None:
                print "No data found for App"
            else:
                prod_images = app_data['prod_images']
                for index,title in enumerate(app_titles):
                    prod_images[index]['title'] = title
                app_data['app_name'] = app_json['app_name']
                app_data['prod_images'] = prod_images
                result = requests.patch(FIREBASE_DB + package + ".json",
                                      data=json.dumps(app_data))
                if result.status_code == 200:
                    self.write_message("App Titles and App Name successfully Updated.")


# -----------------------------PUBLISHED APP HANDLER---------------------------------------------

# --------------------------------------------QUEUE------------------------------------------
class QueueHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in QUEUE and pass to home page
        apps = get_subdirectories(QUEUE_DIR)
        self.render("queue-home.html", apps=apps)

    def post(self):
        print "Success:", self.request.arguments
# ---------------------------QUEUE----CREATE APP-----------------------------------------------
class QueueCrtAppHandler(tornado.web.RequestHandler):
    def get(self):
        app_database = requests.get(FIREBASE_DB + "app_database.json").content
        app_database = json.loads(str(app_database).decode('utf-8'))

        if app_database is None:
            app_database = {}
        else:
            # -----Delete the names that dont start with COM_
            for key in app_database.keys():
                if "com_" not in key:
                    del app_database[key]

        self.render("crt-app.html", app_database=app_database)

# -------------------------QUEUE-UPLOAD IMAGES----------------BEGIN--------------------------------
class ImgUploadHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        app_name = self.request.arguments['app_name'][0]
        self.render("upload-img.html", app_name=app_name)

    def post(self):
        app_name = self.request.arguments['app_name'][0]
        file1 = self.request.files['file'][0]
        original_fname = file1['filename']
        UPLOAD_DIR = os.path.join(ROBOT_DIR,QUEUE_DIR,app_name,ORIGINAL_DIR_STRING)
        if os.path.isdir(UPLOAD_DIR):
            IMG_NAME = os.path.join(UPLOAD_DIR, original_fname)
            output_file = open( IMG_NAME, 'wb')
            output_file.write(file1['body'])

            self.finish("file " + original_fname + " is uploaded")
        else:
            print "DIR Not found:", UPLOAD_DIR
# -------------------------QUEUE-UPLOAD IMAGES----------------END--------------------------------

class QueueAppHandler(tornado.web.RequestHandler):
    def get(self):
        # Get the list of directories in QUEUE and pass to home page
        app_name = self.request.arguments['app_name'][0]
        self.render("queue-app.html", app_name=app_name)


class ValidationHandler(tornado.web.RequestHandler):
    def get(self):
        print self.request.arguments
        app_name = self.request.arguments['app_name'][0]
        # Rename the images in original images directory
        fnRename_images(app_name, QUEUE_DIR)
        # Crete the directories for the App First Then Load the Page
        create_dir(app_name, VALID_DIR_STRING)
        create_dir(app_name, WATERMARK_DIR_STRING)
        create_dir(app_name, TEXT_DIR_STRING)
        create_dir(app_name, FACE_DIR_STRING)
        create_dir(app_name, SET_IMAGE_FOLDER)
        image_limit = 1
        imagelist = getImageURLs(app_name, image_limit)
        # Display Error Message when no images found
        if len(imagelist) == 0:
            self.render("NoImages.html")
        else:
            # Finally render the images in the validation page
            self.render("validation_page.html", imagelist=getImageURLs(app_name, image_limit))

# -------------------------WEB SERVER STARTS HERE ----------------------------------------------
def main():

    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(PORT, HOST_IP)
    print "Listening on {0}:{1}".format(HOST_IP, PORT)
    tornado.ioloop.IOLoop.current().start()

if __name__ == "__main__":
    main()