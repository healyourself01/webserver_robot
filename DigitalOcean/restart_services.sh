#!/usr/bin/env bash

echo "disabling the Firewall. Else Nginx doesn't work"
ufw disable
echo "restarting Minio1"
docker restart minio1
echo "Restarting Nginx"
service nginx restart
echo "restarting couchdb"
systemctl restart couchdb

