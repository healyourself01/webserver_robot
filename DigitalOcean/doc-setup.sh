#!/usr/bin/env bash
# ---Author:Ajay Soma
# name: doc-setup.sh
# Date: Sept-03-2017
#desc: This script installs and configures
#    Minio -- port:9000 -- runs on docker
#    Nginx -- port:9010 -- Load balancer for Minio -- runs on OS
#    couchdb --port:4595 -- runs on OS

#---------------------------Minio setup-----------------------
#    port - 9000
#    name - minio1
#  https://docs.minio.io/docs/minio-docker-quickstart-guide
#
# how to run the script
# curl https://bitbucket.org/healyourself01/webserver_robot/raw/23100cd3da695fb27036d07ca5e35a868a127f9e/DigitalOcean/doc-setup.sh   > doc-setup.sh
#  chmod 755 doc-setup.sh
#  ./doc-setup.sh 139.59.46.214
# IP - Prod- 139.59.46.214
#How to Install Docker
#https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-getting-started

HOST_IP = "139.59.55.102"
echo "Setup running for public IP:139.59.55.102"

echo "disabling the Firewall. Else Nginx doesn't work"
ufw disable

echo "-STEP1------------------------Running Minio docker container--------------------"
docker run -d -p 9000:9000 --name minio1 \
  --restart unless-stopped \
  -e "MINIO_ACCESS_KEY=ajaykumar.soma" \
  -e "MINIO_SECRET_KEY=arunamma" \
  -v /mnt/data:/minio1_data \
  -v /mnt/config:/root/.minio \
  minio/minio server /minio1_data

#mc config host add minio1 http://139.59.55.102:9000 ajaykumar.soma arunamma
#-----------------------Nginx Setup----------------------------
  # https://docs.minio.io/docs/setup-nginx-proxy-with-minio
  #https://www.nginx.com/blog/enterprise-grade-cloud-storage-nginx-plus-minio/
# the config file will be downloaded from our repo
echo "--STEP2---------------INSTALLING NGINX--------------------------------"
sudo apt-get update
sudo apt-get install nginx

echo "--------------deleting the default from nginx folder--------------"
echo "creating cache folder"
mkdir /nginx_cache
cd /etc/nginx/sites-enabled
rm -f -r default
echo "downloading the config from bitbucket"
curl https://bitbucket.org/healyourself01/webserver_robot/raw/9b650bd0fb89ba6b19bec47f23aa499c568d770b/DigitalOcean/default > default
echo "restarting minio"
docker restart minio1
echo "restarting Nginx"
sudo service nginx restart
echo "------TESTING MINIO ON 9000-----------"
curl http://139.59.55.102:9000
echo "------TESTING NGINX ON 9010-----------"
curl http://139.59.55.102:9010
#----------------------------COUCHBASE INSTALLATION-----------------------------------
#-https://www.digitalocean.com/community/tutorials/how-to-install-couchdb-and-futon-on-ubuntu-14-04#step-1-—-preparing-the-server
echo "--STEP4-------------------------------COUCHBASE SETUP STARTING......"
sudo apt-get install software-properties-common -y
sudo add-apt-repository ppa:couchdb/stable -y
sudo apt-get update
#sudo apt-get remove couchdb couchdb-bin couchdb-common -yf -- Removes existing db
sudo apt-get install couchdb -y
echo "creating database robot01"
curl -X PUT 139.59.55.102:5984/production
curl -X PUT 139.59.55.102:5984/test
echo "changing the host name in REST API"
#More info - http://docs.couchdb.org/en/2.0.0/config/intro.html
curl -X PUT http://localhost:5984/_config/httpd/bind_address -d '"0.0.0.0"'
echo "COUCHBASE SETUP COMPLETED SUCCESSFULLY"
echo "------TESTING COUCHBASE-----------"
curl http://139.59.55.102:5984





