import fileinput
import sys
import os, requests
import shutil
import fnmatch
import json
from openpyxl import load_workbook
import subprocess
from AndromedaPy import logger_andromeda
from subprocess import PIPE
from Andromeda_Config import PORT, HOST_IP, FIREBASE_DB, COUCH_DB, BUCKET_BASE, MINIO_BUCKET
from bs4 import BeautifulSoup
# Application constants
from APP_CONFIG import *
import os
import stat
import shutil, jsonmerge
import AndromedaPy as AP
from pprint import pprint

# ------------------------------PUT TO FIREBASE-----------------------------------------------------
def firebase_put(app_json, data):
    package_url = FIREBASE_DB + str(app_json['app_name']).strip() + ".json"
    document_url = COUCH_DB + str(app_json['app_name']).strip()
    result = requests.patch(package_url, json.dumps(data), headers=headers)
    if result.status_code == 200:
        app_json['message'] = "Status Code:{0}  Firebase Update/Write was Successfull.".format(result.status_code)
    else:
        app_json['message'] = "Status code: {0}, Error Message:{1}".format(result.status_code, result.content)
    print app_json['message']
    FIREBASE_URL_UPDATED = FIREBASE_DB + str(app_json['app_name']).strip() + "/" + str(data.keys()[0]).strip() + ".json"
    # app_json['message'] = '<a href= "{0}">View Updated JSON</a>'.format(FIREBASE_URL_UPDATED)
    app_json['message'] = '{0}'.format(FIREBASE_URL_UPDATED)
    print app_json['message']


    #---------------------COUCH DB---------------------------------------------------#
    old_data = requests.get(document_url) #--Read the document first
    if old_data.status_code == 200:
        data["_rev"] = json.loads(old_data.content)["_rev"]  #Merge the data
        # result_couch = requests.(document_url, data=json.dumps(data))
        result_couch = requests.put(document_url, data=json.dumps(data))
        if result_couch.status_code == 200 or result_couch.status_code == 201:
            app_json['message'] = "Status Code:{0}  CouchDB Update/Write was Successfull.".format(result_couch.status_code)
        else:
            app_json['message'] = "Status code: {0}, Error Message:{1}".format(old_data.status_code, old_data.content)
    elif old_data.status_code == 404:
        result_couch = requests.put(document_url, data=json.dumps(data))
        if result_couch.status_code == 200 or result_couch.status_code == 201:
            app_json['message'] = "Status Code:{0}  CouchDB Update/Write was Successfull.".format(result_couch.status_code)
        else:
            app_json['message'] = "Status code: {0}, Error Message:{1}".format(old_data.status_code, old_data.content)

    print app_json['message']
    app_json['message'] = '{0}'.format(document_url)
    print app_json['message']

def replace_bucket(inputurl):
    result = str(inputurl).replace("https://storage.googleapis.com/robot_01/", BUCKET_BASE)
    result = str(inputurl).replace("production//", "production/")
    return result


FROM_DIR = "C:\Users\Ajay\OneDrive\webserver\static\production_robot_apps\InProcess"

app_list = os.listdir(FROM_DIR)
app_list = ["com_mindgame_mehandi_rangoli_app01"]

# f = open("minio_batch_upload.bat", "w")
for index, app in enumerate( app_list):

    # if "maggam" not in app:
    #     continue
    # if index < 85:
    #     continue

    print "Index:",index,"app:", app
    app_json = {'app_name': app}
    package_url = FIREBASE_DB + str(app_json['app_name']).strip() + ".json"
    BUCKET_PROD = BUCKET_BASE + app_json['app_name'] + "/"
    result = requests.get(package_url)
    if result.status_code == 200:
        data = json.loads(result.content)

        if  data is None or "prod_images" not in data.keys():
            continue

        for inner_index, item in enumerate(data["prod_images"]):
            # print "---BEFORE UPDATE---"
            # pprint(item)
            try:
                data["prod_images"][inner_index]["bucket"] = BUCKET_PROD
                data['prod_images'][inner_index]['set_image_url'] = replace_bucket(data['prod_images'][inner_index]['set_image_url'])
            except KeyError:
                print "KeyError for:", app ,"exact:", "index", inner_index
                # print "Index Error for:", app ,"exact:", data["prod_images"], "index", index
                continue
            except IndexError:
                print "Index Error for:", app, "exact:", "index", inner_index
                # print "Index Error for:", app, "exact:", data["prod_images"], "index", index
                continue
            # print "---AFTER UPDATE---"
            # pprint(data["prod_images"][index])
        firebase_put(app_json, data)
    else:
        print "could not find:",package_url




# for index, app in enumerate( app_list):
#     print "Index:",index,"app:", app
#     LOCAL_DIR = os.path.join(ROBOT_DIR, "InProcess", app)
#     LOCAL_DIR = LOCAL_DIR.replace("//", "\\")
#     # gcloud_upload_cmd = ["mc", "cp", "-r", LOCAL_DIR, MINIO_BUCKET]
#     gcloud_upload_cmd = "mc " + " cp " + " -r " + LOCAL_DIR + " " + MINIO_BUCKET
#     print gcloud_upload_cmd
#     f.write(str(gcloud_upload_cmd))
#     f.write("\n")
# f.close()
