import os, requests, json, urllib2, time, urllib3
import APP_CONFIG as CG
from socket import error

# Search the string and print the line if it matches
def search_str(file,srch_str):
    file = str(file).replace("\\", "//")
    input_file = open(file, 'r')
    count_lines = 0
    for line in input_file:
        if srch_str in line:
            print ('------',file,'--------')
            print (line)
        count_lines += 1
#     print ('number of lines:', count_lines)

PREV_RETURN_CODE =True
ERROR_COUNT = 0

def set_err_count(CURR_RETURN_CODE):
    global ERROR_COUNT, PREV_RETURN_CODE
    if (CURR_RETURN_CODE is False) and (CURR_RETURN_CODE == PREV_RETURN_CODE):
        ERROR_COUNT += 1
    if (CURR_RETURN_CODE is False) and (CURR_RETURN_CODE <> PREV_RETURN_CODE):
        ERROR_COUNT = 0
    PREV_RETURN_CODE = CURR_RETURN_CODE


def download_img_from_url(img_url, dir_path, img_name):
        global PREV_RETURN_CODE, CURR_RETURN_CODE
        if not os.path.isfile(os.path.join(dir_path, img_name)):
            try:
                req = urllib2.Request(img_url, headers={'User-Agent': CG.HEADERS_SCRAPER})
                print ("Downloading Image:",dir_path, img_name)
                raw_img = urllib2.urlopen(req).read()
                File = open(os.path.join(dir_path, img_name), "wb")
                File.write(raw_img)
                File.close()
                time.sleep(0.2)  # Give 1 sec Break
                PREV_RETURN_CODE = True
                return True
            except urllib2.HTTPError as e:
                if e.code == 404:
                    print ("******404 Downloading Image:", img_url)
                    set_err_count(False)
                    return False

                if e.code == 502: #Bad gateway
                    print ("******502 Downloading Image:",img_url)
                    raw_img = urllib2.urlopen(req).read()
                    File = open(os.path.join(dir_path, img_name), "wb")
                    File.write(raw_img)
                    File.close()
                    time.sleep(0.2)  # Give 1 sec Break
                    PREV_RETURN_CODE =True
                    return True
                print ("******Failed to Downloading Image:", img_url)
            except error:
                print ("******Socket error:", img_url)
                # Wait for 10 sec when socket error
                time.sleep(10)
            finally:
                set_err_count(False)
                return False

        else:
            print "Image Already present:", os.path.join(dir_path, img_name)
            PREV_RETURN_CODE = True
            return True


img_url = "https://f001.backblazeb2.com/file/robot01-prod/img1.jpg"
items = range(1,5000)

OUT_RANGOLI = "D://Andomeda Robot//BackBlaze"

for item in items:
    print ('-------------curr item:',item, '-------------------------')
    img_name = "img" + str(item)+".jpg"
    result = download_img_from_url(img_url=img_url,dir_path=OUT_RANGOLI,img_name=img_name)

