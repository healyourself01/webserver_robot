import logging, unicodedata
import os, shutil, subprocess
from subprocess import PIPE
from Andromeda_Config import HOST_IP, PORT
from APP_CONFIG import *
import APP_CONFIG as CG
from datetime import datetime
import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import os
import stat
import shutil
# ******************************************CONSTANTS************************************


# ******************************************CONSTANTS************************************
# http://stackoverflow.com/questions/1889597/deleting-directory-in-python
def _remove_readonly(fn, path_, excinfo):
    # Handle read-only files and directories
    if fn is os.rmdir:
        os.chmod(path_, stat.S_IWRITE)
        os.rmdir(path_)
    elif fn is os.remove:
        os.lchmod(path_, stat.S_IWRITE)
        os.remove(path_)


def force_remove_file_or_symlink(path_):
    try:
        os.remove(path_)
    except OSError:
        os.lchmod(path_, stat.S_IWRITE)
        os.remove(path_)


# Code from shutil.rmtree()
def is_regular_dir(path_):
    try:
        mode = os.lstat(path_).st_mode
    except os.error:
        mode = 0
    return stat.S_ISDIR(mode)


# -----------------conv_ascii-----------------------------------------------------------
def conv_ascii(str_in):
    try:
        str_out = unicodedata.normalize('NFKD', str_in).encode('ascii', 'ignore')
        return str(str_out).strip()
    except UnicodeEncodeError:
        print "UnicodeEncodeError"
        return "UnicodeEncodeError"

# -----------------scrape_validation-----------------------------------------------------------
def scrape_validation(date_in):
    try:
        if date_in is not None:
            date_last = datetime.strptime(date_in, "%Y-%m-%d")
            date_curr = datetime.strptime(CG.CURRENT_DATE, "%Y-%m-%d")
            delta = date_curr - date_last

            if delta.days >= CG.DaysDelta:
                return True
            else:
                return False
        else:
            return True

    except UnicodeEncodeError:
        pass

# -----------------------------------------get_dir_list----BEGINNING-----------------------------
def get_subdirectories(INPUT_DIR):
    dir_list = []
    item_list = os.listdir(os.path.join(ROBOT_DIR,INPUT_DIR))
    for dir_item in item_list:
        if os.path.isdir(os.path.join(ROBOT_DIR,INPUT_DIR,dir_item)) and dir_item != "\\":
            dir_list.append(dir_item)
    return dir_list
# -----------------------------------------get_dir_list----END-----------------------------
# -----------------------------------------MONGO DB EXISTS---------------------------------
# Input - Collection, Key(ID)
# If exists return true else return False
def mongo_exists(collection, key):
    data = db[collection].find_one({"_id": key})
    if data is None:
        return False
    else:
        return True
# -----------------------------------------MONGO DB WRITE---------------------------------
# Input - Collection, Key(ID)
# If exists return true else return False
def mongo_write(collection, key, document):
    if not mongo_exists(collection,key):
        document["_id"] = str(key)
        result = db[collection].insert_one(document).inserted_id
        return "Inserted document with Key:", result
    else:
        return "Key:{0} Already exists.".format(key)
# --------------------------MONGO UPDATE------BEGIN--------------------------------------
def mongo_update(collection, key, document):
    if mongo_exists(collection,key):
        db[collection].update_one(
        {'_id': key},
        {'$set': document }, upsert=False)
        return "Updated Key:{0}".format(key)
    else:
        return "Key:{0} Does Not exist.".format(key)
# --------------------------MONGO UPDATE-------END---------------------------------------

# --------------------------MONGO DELETE-------BEGIN---------------------------------------
def mongo_delete(collection, key):
    if mongo_exists(collection,key):
        db[collection].delete_many(
        {'_id': key})
        return "Deleted Key:{0}".format(key)
    else:
        return "Key:{0} Does Not exist.".format(key)
# --------------------------MONGO DELETE-------END---------------------------------------
# --------------------------MONGO READ-------BEGIN---------------------------------------
def mongo_read(collection, qstring):
    if qstring is None:
        qstring = {}

    data = [{},{}]
    data = list( db[collection].find(qstring) )
    if data is not None:
        return data
    else:
        print "Qstring:{0} Does Not exist.".format(qstring)
        return []


# --------------------------MONGO READ-------END---------------------------------------
# --------------------------MONGO DISTINCT READ-------BEGIN---------------------------------------
def mongo_read_distinct(collection, qstring, field):
    if qstring is None:
        qstring = {}

    data = [{},{}]
    data = list( db[collection].find(qstring).distinct(field) )
    if data is not None:
        return data
    else:
        print "Qstring:{0} Does Not exist.".format(qstring)
        return []


# --------------------------MONGO DISTINCT READ-------END---------------------------------------
# -----------------------------------------ABSOLUTE URL---------------------------------
def absolute_url(*params):
    abs_url = "http://" + HOST_IP + ":" + str(PORT)
    for ele in params:
        abs_url = abs_url + ele
    return abs_url

# ------------------------------------------LOGGER--------------------------------------
def logger_andromeda(app_json, clear):

    LOG_FILENAME = os.path.join(ROBOT_DIR, str(app_json['stage_dir']), str(app_json['app_name']),
                                str(LOG_DIR), str(app_json['action']+"_log.txt"))

    print LOG_FILENAME

    if clear == True:
        if os.path.exists(LOG_FILENAME):
            os.remove(LOG_FILENAME)
        return
    else:
        try:
            # logging.basicConfig(filename=LOG_FILENAME, filemode="w", level=logging.INFO)
            logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)
            # logging.debug(app_json['message'])
            logging.debug("Hello World")

            f = open(LOG_FILENAME, "a")
            f.write(str(app_json['message']) + "\n")
            f.close()
            print app_json['message']
            app_json['self'].write_message(app_json['message'])
        except IOError:
            app_json['message'] = "Error writing to log file."
            app_json['self'].write_message(app_json['message'])

# -----------------------Julian Date Conversion--------------------------
def con_date_julina(date_in):
    import sqlite3
    con = sqlite3.connect(":memory:")
    # list(con.execute("SELECT julianday('2017-02-01')"))[0][0]
    list(con.execute("SELECT julianday(" + date_in + ")"))[0][0]