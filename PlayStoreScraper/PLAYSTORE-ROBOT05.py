
from selenium import webdriver
# --------SLENIUM EXCEPTIONS
from selenium.common.exceptions import NoSuchElementException, WebDriverException


def check_exists_by_xpath(xpath):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True


driver = webdriver.Firefox()

url = "https://play.google.com/store/apps"

driver.get(url)

search = driver.find_element_by_id("gbqfq")
search.click()
search.send_keys("mehndi")
