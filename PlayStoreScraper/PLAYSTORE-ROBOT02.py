import os
import csv
import time, requests
from bs4 import BeautifulSoup
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import AndromedaPy as AP
import Andromeda_Config as AC
import APP_CONFIG as CG
import Scraper_Functions as SP
import DataDescriptions as DD
from datetime import datetime

# --------SLENIUM EXCEPTIONS
from selenium.common.exceptions import NoSuchElementException, WebDriverException



# profile = webdriver.FirefoxProfile(AC.FIREFOX_PROFILE)
# profile.set_preference('browser.download.folderList', 2)
# profile.set_preference('browser.download.manager.showWhenStarting', False)
# profile.set_preference('browser.download.dir')
# profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))
# driver = webdriver.Firefox()
# wait = WebDriverWait(driver, 10)


# ---------------------LOOP00---------------------------------
# Loop through al the list of urls from starturls kind
def robot02_run():

    query = CG.db[DD.COLLECTION_02_PLAPHDR].find({})

    TIMESTAMP_START = datetime.now()
    for app_hdr in query:
        app_dtl = dict()
        # ---GET APP DATA
        if AP.scrape_validation(app_hdr[DD.PLAPHDR_PLAHR02LRDT_03]) is True\
                or AP.mongo_exists(DD.COLLECTION_03_PLAPDTL, app_hdr["_id"]) is False:
            package = app_hdr['_id']
            print "Processing:", package
            app_url = app_hdr[DD.PLAPHDR_PLAHURL_01]
            page = requests.get(url=app_url, headers= CG.HEADERS_SCRAPER)
            if page.status_code == 200:
                soup = BeautifulSoup(page.content, 'html.parser',from_encoding="utf-8")
                time.sleep(CG.REQUESTS_TIME_DELTA) #Delay
                app_dtl = SP.get_app_data(soup, url=app_url)
                app_dtl[DD.PLAPDTL_PLADR02LRDT_03] = CG.CURRENT_DATE
                app_dtl[DD.PLAPDTL_PLADSRC_02] = app_hdr[DD.PLAPHDR_PLAHSRC_02] #Source string
                app_dtl[DD.PLAPDTL_PLADR02STS_04] = {CG.CURRENT_DATE:False}

                if AP.mongo_exists(DD.COLLECTION_03_PLAPDTL, package) is True:
                    print AP.mongo_update(DD.COLLECTION_03_PLAPDTL, package, app_dtl)
                else:
                    print AP.mongo_write(DD.COLLECTION_03_PLAPDTL, package, app_dtl)
                # ---------UPDATE PLAPHDR file
                app_hdr[DD.PLAPHDR_PLAHR02LRDT_03] = CG.CURRENT_DATE  # Last Run Date for R02
                if app_hdr[DD.PLAPHDR_PLAHR02STS_04] is not None:
                    app_hdr[DD.PLAPHDR_PLAHR02STS_04][CG.CURRENT_DATE] = True
                print AP.mongo_update(DD.COLLECTION_02_PLAPHDR, app_hdr['_id'], app_hdr)
            else:
                print "App:{0} in Error:{1}".format(app_url, page.status_code)
                print AP.mongo_delete(DD.COLLECTION_02_PLAPHDR, app_hdr["_id"])

                    # --------------------------- LOOP02 -----------------------------------------
        # update the link that it has been crawled for today

    TOTAL_TIME_TAKEN = datetime.now() - TIMESTAMP_START
    print "total time:{0}, TotalSec:", TOTAL_TIME_TAKEN, TOTAL_TIME_TAKEN.total_seconds()
        # close the browser
    # driver.quit()

robot02_run()