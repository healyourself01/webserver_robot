def import_file(full_path_to_module):
    try:
        import os
        module_dir, module_file = os.path.split(full_path_to_module)
        module_name, module_ext = os.path.splitext(module_file)
        save_cwd = os.getcwd()
        os.chdir(module_dir)
        module_obj = __import__(module_name)
        module_obj.__file__ = full_path_to_module
        globals()[module_name] = module_obj
        os.chdir(save_cwd)
    except:
        raise ImportError

import_file("C:\Users\Ajay\OneDrive\TestBox\webserver-TEST\PlayStoreScraper\PLAYSTORE-ROBOT00.py")
import_file("C:\Users\Ajay\OneDrive\TestBox\webserver-TEST\PlayStoreScraper\PLAYSTORE-ROBOT01.py")
import_file("C:\Users\Ajay\OneDrive\TestBox\webserver-TEST\PlayStoreScraper\PLAYSTORE-ROBOT02.py")
import_file("C:\Users\Ajay\OneDrive\TestBox\webserver-TEST\PlayStoreScraper\PLAYSTORE-ROBOT03.py")

