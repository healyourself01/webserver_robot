import os
import csv
import time, requests
from bs4 import BeautifulSoup
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import AndromedaPy as AP
import Andromeda_Config as AC
import APP_CONFIG as CG
import Scraper_Functions as SP
import DataDescriptions as DD

# --------SLENIUM EXCEPTIONS
from selenium.common.exceptions import NoSuchElementException, WebDriverException


def check_exists_by_xpath(xpath):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True


# profile = webdriver.FirefoxProfile(AC.FIREFOX_PROFILE)
# profile.set_preference('browser.download.folderList', 2)
# profile.set_preference('browser.download.manager.showWhenStarting', False)
# profile.set_preference('browser.download.dir')
# profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))
driver = webdriver.Firefox()
wait = WebDriverWait(driver, 10)

# ---------------------LOOP00---------------------------------
# Loop through al the list of urls from PLINLNKS Collection

def robot01_run():

    query = CG.db[DD.COLLECTION_01_PLINLNKS].find({})

    for link in query:
        if AP.scrape_validation(link[DD.PLINLNKS_PLLR01LRDT_03]) is True:
            wait = WebDriverWait(driver, 3)
            print link[DD.PLINLNKS_PLLURL_01]
            driver.get(str(link[DD.PLINLNKS_PLLURL_01]))
            # -------------LOOP01-------------------------------
            # Scroll to the bottom of the page for 10 times
            # Then only all the apps will come up in the page
            scroll_count = 0
            while scroll_count <=5:
                # *************Need to click on show more button
                driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
                time.sleep(2)
                scroll_count += 1
                try:
                    driver.find_element_by_id("show-more-button").click()
                    print "Clicked on show more."
                    scroll_count = 1 #Again need to scroll down if show more button is found.
                except NoSuchElementException:
                    pass
                except WebDriverException:
                    pass

            # xpath - //a[@class='title'] - select all the anchor tags with class = title
            # android_apps_found = driver.find_elements_by_xpath("//a[@class='title']")
            android_apps_found = driver.find_elements_by_class_name("card-content")
            source_str = link[DD.PLINLNKS_PLLSRC_02]['source'] + "." + link[DD.PLINLNKS_PLLSRC_02]['value']

            total_apps = len(android_apps_found)
            print "Total Apps -  Source:{0} : Count:{1}".format(str(link[DD.PLINLNKS_PLLSRC_02]), total_apps)
            #--------------------------- LOOP02 -----------------------------------------
            # Main Loop to extract all the title elements
            for ele in android_apps_found:
                app_ele = ele.find_element_by_class_name('title')
                try:
                    dev_ele = ele.find_element_by_class_name('subtitle')
                except NoSuchElementException:
                    dev_ele = None

                aphdr = dict()
                aphdr['_id'] = package = AP.conv_ascii(app_ele.get_attribute('href')).split("=")[1].decode('utf-8').strip() #PACKAGE
                aphdr[DD.PLAPHDR_PLAHURL_01] = AP.conv_ascii(app_ele.get_attribute('href'))       #URL
                aphdr[DD.PLAPHDR_PLAHTTL_02] = AP.conv_ascii(app_ele.text)                 #App Title
                try:
                    aphdr[DD.PLAPHDR_PLAHDEV_02] = AP.conv_ascii(dev_ele.get_attribute('href')).split("=")[1].decode('utf-8').strip()  # Developer Name
                except:
                    aphdr[DD.PLAPHDR_PLAHDEV_02] = None
                aphdr[DD.PLINLNKS_PLLSRC_02] = link[DD.PLINLNKS_PLLSRC_02]  # Source
                aphdr[DD.PLAPHDR_PLAHSRC_02] = source_str
                aphdr[DD.PLAPHDR_PLAHR02LRDT_03] = None
                aphdr[DD.PLAPHDR_PLAHR02STS_04] = {CG.CURRENT_DATE: False}

                print AP.mongo_write(DD.COLLECTION_02_PLAPHDR, package, aphdr)

            # --------------------------- LOOP02 -----------------------------------------
            # update the link that it has been crawled for today
            link[DD.PLINLNKS_PLLR01LRDT_03] = CG.CURRENT_DATE #Last Run Date for R01
            link[DD.PLINLNKS_PLLTOTAP_05] = total_apps #Total App Count
            if link[DD.PLINLNKS_PLLR01STS_04] is not None:
                link[DD.PLINLNKS_PLLR01STS_04][CG.CURRENT_DATE] = True
            print AP.mongo_update(DD.COLLECTION_01_PLINLNKS, link['_id'], link)


    # close the browser
    driver.quit()

robot01_run()