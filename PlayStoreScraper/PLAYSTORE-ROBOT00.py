import os
import csv
import time, requests, json
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import Andromeda_Config
import APP_CONFIG as AC
import AndromedaPy as AP
import DataDescriptions as DD
import APP_CONFIG as CG
import Scraper_Functions as SF
import json
#
# it reads the keywords and developers list from the kewords kind and
# builds the list of URLs to crawl the playstore
#
#


# ---Read the developers list from Firebase
# page = requests.get(AC.FIREBASE_DDEVELOPER_LIST)
# if page.status_code == 200 and page.content != "null":
#     developers = json.loads(page.content)



def robot00_run():
    PLDEVLST_DATA = AP.mongo_read(collection=DD.COLLECTION_00_PLDEVLST, qstring=None)
    PLKWDLST_DATA = AP.mongo_read(collection=DD.COLLECTION_00_PLKWDLST, qstring=None)

    for DEV in PLDEVLST_DATA:
        dev_name = DEV[DD.PLDEVLST_PLDDID_01]
        if str(dev_name).isdigit() is True:
            url = CG.DEVELOPER_BASE_URL_INT + str(dev_name)
        else:
            url = CG.DEVELOPER_BASE_URL_STRING + str(dev_name)

        print url
        # Prepares the new entity
        link = dict()
        source = dict()
        source['source'] = "PLDEVLST"
        source['value'] = str(DEV[DD.PLDEVLST_PLDDID_01]).strip()
        # Prepare Data
        link['_id'] = str(DEV[DD.PLDEVLST_PLDDID_01]).strip()
        link[DD.PLINLNKS_PLLURL_01] = str(url).strip()
        link[DD.PLINLNKS_PLLSRC_02] = source
        link[DD.PLINLNKS_PLLR01LRDT_03] = None
        link[DD.PLINLNKS_PLLR01STS_04] = {CG.CURRENT_DATE: False}
        print AP.mongo_write(DD.COLLECTION_01_PLINLNKS, DEV[DD.PLDEVLST_PLDDID_01], link)

    for KWD in PLKWDLST_DATA:
        url = CG.QUERY_BASE_HEAD + KWD[DD.PLKWDLST_PLKKID_01] + CG.QUERY_BASE_TAIL
        print url
        # Prepares the new entity
        link = dict()
        source = dict()
        source['source'] = "PLKWDLST"
        source['value'] = str(KWD[DD.PLKWDLST_PLKKID_01]).strip()
        # Prepare Data
        link['_id'] = str(KWD[DD.PLKWDLST_PLKKID_01]).strip()
        link[DD.PLINLNKS_PLLURL_01] = str(url).strip()
        link[DD.PLINLNKS_PLLSRC_02] = source
        link[DD.PLINLNKS_PLLR01LRDT_03] = None
        link[DD.PLINLNKS_PLLR01STS_04] = {CG.CURRENT_DATE: False}
        print AP.mongo_write(DD.COLLECTION_01_PLINLNKS, KWD[DD.PLKWDLST_PLKKID_01], link)


robot00_run()
