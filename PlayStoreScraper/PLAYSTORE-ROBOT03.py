import os
import csv
import time, requests
from bs4 import BeautifulSoup
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import AndromedaPy as AP
import Andromeda_Config as AC

import Scraper_Functions as SF
import DataDescriptions as DD
from datetime import datetime
from dateutil import relativedelta

# --------SLENIUM EXCEPTIONS
from selenium.common.exceptions import NoSuchElementException, WebDriverException
import DataDescriptions as DD
import AndromedaPy as AP
import APP_CONFIG as CG



# ---------------------LOOP00---------------------------------
# Loop through al the list of urls from PLAPDTL
def robot03_run():
    query = CG.db[DD.COLLECTION_03_PLAPDTL].find({})

    TIMESTAMP_START = datetime.now()
    for app_dtl in query:
        package = app_dtl["_id"]
        installs = app_dtl[DD.PLAPDTL_PLADINST_05]
        app_date = app_dtl[DD.PLAPDTL_PLADDAT_06]
        app_dtl[DD.PLAPDTL_PLADMNINS_13] = SF.get_min_installs(installs)
        app_dtl[DD.PLAPDTL_PLADMXINS_14] = SF.get_max_installs(installs)
        app_dtl[DD.PLAPDTL_PLADJULD_15] = SF.conv_date_julian(app_date)
        app_dtl[DD.PLAPDTL_PLADINPD_16] = SF.installs_per_day(SF.get_max_installs(installs), SF.dates_diff_days(app_date))
        app_dtl[DD.PLAPDTL_PLADTDYS_17] = SF.dates_diff_days(app_date)
        app_dtl[DD.PLAPDTL_PLADTWKS_19] = SF.dates_diff(app_date).weeks
        app_dtl[DD.PLAPDTL_PLADTMNT_18] = SF.dates_diff(app_date).months

        if AP.mongo_exists(DD.COLLECTION_04_PLAPDTLA01, package) is True:
            AP.mongo_update(DD.COLLECTION_04_PLAPDTLA01, package, app_dtl)
        else:
            AP.mongo_write(DD.COLLECTION_04_PLAPDTLA01, package, app_dtl)

    TOTAL_TIME_TAKEN = datetime.now() - TIMESTAMP_START
    print "total time:{0}, TotalSec:", TOTAL_TIME_TAKEN, TOTAL_TIME_TAKEN.total_seconds()
        # close the browser
    # driver.quit()

robot03_run()