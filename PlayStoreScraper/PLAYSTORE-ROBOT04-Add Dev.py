import os
import csv
import time, requests, json
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import Andromeda_Config
import APP_CONFIG as AC
import AndromedaPy as AP
import DataDescriptions as DD
import APP_CONFIG as CG
import Scraper_Functions as SF
import json
#
# it reads the keywords and developers list from the kewords kind and
# builds the list of URLs to crawl the playstore
#
#


# ---Read the developers list from Firebase
# page = requests.get(AC.FIREBASE_DDEVELOPER_LIST)
# if page.status_code == 200 and page.content != "null":
#     developers = json.loads(page.content)

PLDEVLST_DATA = AP.mongo_read(collection=DD.COLLECTION_00_PLDEVLST, qstring=None)
PLKWDLST_DATA = AP.mongo_read(collection=DD.COLLECTION_00_PLKWDLST, qstring=None)

DEVLST = AP.mongo_read_distinct(DD.COLLECTION_02_PLAPHDR, {}, DD.PLAPHDR_PLAHDEV_02)

for developer in DEVLST:
    dev_data = dict()
    dev_data["DevID"] = str(developer)
    dev_data["DevName"] = str(developer)
    print SF.add_developer(dev_data=dev_data)
