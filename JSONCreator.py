import fileinput
import sys
import os, requests
import shutil
import fnmatch
import json
from openpyxl import load_workbook
import subprocess
from AndromedaPy import logger_andromeda
from subprocess import PIPE
from Andromeda_Config import PORT, HOST_IP, FIREBASE_DB, COUCH_DB, BUCKET_BASE, MINIO_BUCKET
from bs4 import BeautifulSoup
# Application constants
from APP_CONFIG import *
import os
import stat
import shutil, jsonmerge
import AndromedaPy as AP
"""
MAKE JSON
"""
APP_NAME = ""
APP_DIR = ""



# ------------------------------------------------------------------
# is_image(filename)
# checks whether the file is image and returns True or False
# ------------------------------------------------------------------


def is_image(filename):
    if os.path.splitext(filename)[1].lower() in IMAGE_FILE_EXTENSIONS and filename <> "thumbs":
        return True
    else:
        return False


# ------------------------------PUT TO FIREBASE-----------------------------------------------------
def firebase_put(app_json, data):
    package_url = FIREBASE_DB + str(app_json['app_name']).strip() + ".json"
    document_url = COUCH_DB + str(app_json['app_name']).strip()
    result = requests.patch(package_url, json.dumps(data), headers=headers)
    if result.status_code == 200:
        app_json['message'] = "Status Code:{0}  Firebase Update/Write was Successfull.".format(result.status_code)
    else:
        app_json['message'] = "Status code: {0}, Error Message:{1}".format(result.status_code, result.content)
    logger_andromeda(app_json, clear=False)
    FIREBASE_URL_UPDATED = FIREBASE_DB + str(app_json['app_name']).strip() + "/" + str(data.keys()[0]).strip() + ".json"
    # app_json['message'] = '<a href= "{0}">View Updated JSON</a>'.format(FIREBASE_URL_UPDATED)
    app_json['message'] = '{0}'.format(FIREBASE_URL_UPDATED)
    logger_andromeda(app_json, clear=False)


    #---------------------COUCH DB---------------------------------------------------#
    old_data = requests.get(document_url) #--Read the document first
    if old_data.status_code == 200:
        new_data = jsonmerge.merge(json.loads(old_data.content), data)  #Merge the data
        result_couch = requests.put(document_url, data=json.dumps(new_data))
        if result_couch.status_code == 200 or result_couch.status_code == 201:
            app_json['message'] = "Status Code:{0}  CouchDB Update/Write was Successfull.".format(result_couch.status_code)
        else:
            app_json['message'] = "Status code: {0}, Error Message:{1}".format(old_data.status_code, old_data.content)
    elif old_data.status_code == 404:
        result_couch = requests.put(document_url, data=json.dumps(data))
        if result_couch.status_code == 200 or result_couch.status_code == 201:
            app_json['message'] = "Status Code:{0}  CouchDB Update/Write was Successfull.".format(result_couch.status_code)
        else:
            app_json['message'] = "Status code: {0}, Error Message:{1}".format(old_data.status_code, old_data.content)

    logger_andromeda(app_json, clear=False)
    app_json['message'] = '{0}'.format(document_url)
    logger_andromeda(app_json, clear=False)


# ------------------------------PATCH TO FIREBASE-----------------------------------------------------
def firebase_patch(app_json,key, data):
    package_url = FIREBASE_DB + str(app_json['app_name']).strip() + "/" + key + ".json"
    result = requests.patch(package_url, json.dumps(data), headers=headers)
    if result.status_code == 200:
        app_json['message'] = "Status Code:{0} for Key:{1} Firebase Patch was Successfull.".format(result.status_code, key)
    else:
        app_json['message'] = "Status code: {0}, Error Message:{1}".format(result.status_code, result.content)
    logger_andromeda(app_json, clear=False)

    #---------------------COUCH DB---------------------------------------------------#
    document_url = COUCH_DB + str(app_json['app_name']).strip()
    old_data = requests.get(document_url) #--Read the document first
    if old_data.status_code == 200:
        new_data = jsonmerge.merge(json.loads(old_data.content), data)  #Merge the data
        result_couch = requests.put(document_url, data=json.dumps(new_data))
        if result_couch.status_code == 200:
            app_json['message'] = "Status Code:{0}  CouchDB Update/Write was Successfull.".format(result_couch.status_code)
        else:
            app_json['message'] = "Status code: {0}, Error Message:{1}".format(old_data.status_code, old_data.content)
    else:
        app_json['message'] = "Status code: {0}, Error Message:{1}".format(old_data.status_code, old_data.content)

    logger_andromeda(app_json, clear=False)


# -----------------------------VALIDATE URL-----------------------------------------------------------
"""
Validates URL, if status code <> 200 report error
"""
def validate_url(app_json, url_to_test):
    result = requests.get(url_to_test)
    if result.status_code <> 200:
        line = "\n" + "---------------------------------------------------------------------------------------" + "\n"
        soup = BeautifulSoup(result.content, "html.parser")

        app_json['message'] = line + url_to_test + "\n" + soup.code.text
        logger_andromeda(app_json, clear=False)
        return False
    elif result.status_code == 200:
        return True

#---------------------------------GET SET IMAGES------------------------------------------------------
"""
get_set_images(set_dir) 
"""
def get_set_images(set_dir):
    image_list =[]
    for item in os.listdir(os.path.join(APP_DIR,set_dir)):
        if is_image(item):
            image_list.append(str(item))
    return image_list
# --------------------------Move All Images-----------------------------------------------------------
"""
move_all_images
"""
def move_all_images(app_dir, stage_dir, from_dir, to_dir):
    for item in os.listdir(os.path.join(ROBOT_DIR, stage_dir, app_dir, from_dir)):
        source_item = os.path.join(ROBOT_DIR, stage_dir, app_dir, from_dir, item)
        destination_item = os.path.join(ROBOT_DIR, stage_dir, app_dir, to_dir, item)
        shutil.move(source_item, destination_item)
    return "All Iamges in App: {0} moved to {1} Successfully.".format(app_dir, to_dir)

# --------------------------Move App-----------------------------------------------------------
"""
move_app 
"""
def move_app(app_dir, from_dir, to_dir):
    source_dir = os.path.join(ROBOT_DIR, from_dir, app_dir)
    destination_dir = os.path.join(ROBOT_DIR, to_dir, app_dir)
    shutil.move(source_dir, destination_dir)
    message = "App: {0} moved to {1} Successfully.".format(app_dir, to_dir)
    return message
# --------------------------Create App-----------------------------------------------------------
"""
create_app 
"""
def create_app(app_json):
    # logger_andromeda(app_json,clear=False)
    app_url = FIREBASE_DB + app_json['package'] + ".json"
    result = requests.get(app_url)
    app_path = os.path.join(ROBOT_DIR,QUEUE_DIR,app_json['package'])
    if result.status_code == 200 and result.content == "null":
        if os.path.isdir(app_path) is False:
            os.mkdir(app_path) #Create App Dir
            os.mkdir(os.path.join(app_path, ORIGINAL_DIR_STRING)) #Create Original Dir
            message = "App: {0} created in {1} Successfully.".format(app_json['package'], QUEUE_DIR)
        else:
            message = "App already exists in QUEUE."
    elif result.status_code == 200 and result.content != "null":
        message = "App already present in Firebase"
    app_json['self'].write_message(message)
    # app_json['message'] = message
    # logger_andromeda(app_json, clear=False)
# --------------------------Create App Dir-----------------------------------------------------------

"""
create_app_dir
"""
def create_app_dir(app_json):
    global APP_DIR
    logger_andromeda(app_json, clear=True)
    print "---------Pocessing App:", APP_NAME, "----------------------------------"
    # -----------------------------Set The App Dir
    APP_DIR = os.path.join(ROBOT_DIR, app_json['stage_dir'], app_json['app_name'])
    # ------------------------------------1.Create Set Dir------------------------------------------
    print "1.Create Set Dir for 10 sets"
    create_dir(SET1_STR)
    create_dir(SET2_STR)
    create_dir(SET3_STR)
    create_dir(SET4_STR)
    create_dir(SET5_STR)
    create_dir(SET6_STR)
    create_dir(SET7_STR)
    create_dir(SET8_STR)
    create_dir(SET9_STR)
    create_dir(SET10_STR)
    print "1.1 Creating App Directories"
    create_dir(VALID_DIR_STRING)
    create_dir(IN_APP_GRAPHICS)
    create_dir(SCREEN_SHOTS)
    create_dir(APK_KEY)
    create_dir(LOG_DIR)
    create_dir(SET_IMAGE_FOLDER)
    app_json['message'] = "Directories for App: {0} created Successfully.".format(app_json['app_name'])
    logger_andromeda(app_json,  clear=False)
    return

# --------------------------------------DELETE ALL IMAGES -----------------------------------------
def clear_dir(path_):

    if len(os.listdir(path_)) == 0:
        return "Directory is Empty"

    if AP.is_regular_dir(path_):
        # Given path is a directory, clear its content
        for name in os.listdir(path_):
            fullpath = os.path.join(path_, name)
            if AP.is_regular_dir(fullpath):
                shutil.rmtree(fullpath, onerror=AP._remove_readonly)
            else:
                AP.force_remove_file_or_symlink(fullpath)
        return "Deleted All Images from:{0}".format(path_)
    else:
        # Given path is a file or a symlink.
        # Raise an exception here to avoid accidentally clearing the content
        # of a symbolic linked directory.
        # return "Unable to Delete:", path_
        raise OSError("Cannot call clear_dir() on a symbolic link")
# --------------------------------------CREATE SET DIR -----------------------------------------
def create_dir(dir_temp):
    path = os.path.join(APP_DIR, dir_temp)
    if not os.path.isdir(path):
        os.mkdir(path)

# --------------------------------------2.Add Base JSON-----------------------------------------
def btnConvertToWEBP(app_json):
    logger_andromeda(app_json, clear=True)
    LOCAL_DIR = os.path.join(ROBOT_DIR, app_json['stage_dir'], app_json['app_name'])
    LOCAL_DIR = LOCAL_DIR.replace("//", "\\")
    LOG_FILE_PATH = os.path.join(LOCAL_DIR, LOG_DIR, str(app_json['action']).strip() + "_log.txt")
    convet_webp = ["webpcnv01", LOCAL_DIR, ">", LOG_FILE_PATH, "2>&1"]
    print convet_webp
    webp_converions_process = subprocess.Popen(convet_webp, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)

    webp_converions_process.communicate(b"input data that is passed to subprocess' stdin")

    f = open(LOG_FILE_PATH)
    webp_conversion_log = str(f.read())
    f.close()
    app_json['message'] = "Uploaded App:{0} To {1}".format(app_json['app_name'],LOCAL_DIR) + "\n" + webp_conversion_log
    logger_andromeda(app_json, clear=False)
    print webp_conversion_log

# --------------------------------------2.Add Base JSON-----------------------------------------
def add_base_json(app_json):
    logger_andromeda(app_json, clear=True)
    json_file = open("C://Users//Ajay//OneDrive//webserver//app_base.json", "r")
    json_base = json.loads(json_file.read())
    json_file.close()
    firebase_put(app_json, data=json_base)

# --------------------------------------3.Set Test Modes-----------------------------------------
def set_test_mode(app_json):
    logger_andromeda(app_json, clear=True)
    modes = {
        "image_mode": "prod",
        "admob_mode": "hold"
    }
    firebase_put(app_json, data=modes)
# --------------------------------------3.Make Sets-----------------------------------------
def make_sets(app_json):
    logger_andromeda(app_json, clear=True)
    global BUCKET_PROD
    global APP_DIR
    APP_DIR = os.path.join(ROBOT_DIR, IN_PROCES_DIR, app_json['app_name'])
    valid_images_list = os.listdir(os.path.join(APP_DIR, VALID_DIR_STRING))
    BUCKET_PROD = BUCKET_BASE + app_json['app_name'] + "/"
    print BUCKET_PROD
    total_images = len(valid_images_list)
    # Min 10 Images should be present in the APP as there are 10 sets
    if total_images < 10:
        print "Min 10 Images Should be present in App: {0}".format(os.path.join(APP_DIR, VALID_DIR_STRING))
        # raise "Min 10 Images Should be present in App"
    # -----------------------------------2.Distribute valid images to sets-------------------------------
    print "2.Distribute valid images to sets"
    images_per_set = round(total_images / 10.0)
    move_valid_images_to_set(images_per_set, SET1_STR)
    move_valid_images_to_set(images_per_set, SET2_STR)
    move_valid_images_to_set(images_per_set, SET3_STR)
    move_valid_images_to_set(images_per_set, SET4_STR)
    move_valid_images_to_set(images_per_set, SET5_STR)
    move_valid_images_to_set(images_per_set, SET6_STR)
    move_valid_images_to_set(images_per_set, SET7_STR)
    move_valid_images_to_set(images_per_set, SET8_STR)
    move_valid_images_to_set(images_per_set, SET9_STR)
    move_valid_images_to_set(images_per_set, SET10_STR)

    #-----------------Convert Images to Webp----------------------------------------------------
    # btnConvertToWEBP(app_json)

    # ---------------------------------3. Create JSON for app ------------------------------------------
    print "3. Create JSON for app"
    set1_images = get_set_images(SET1_STR)
    set2_images = get_set_images(SET2_STR)
    set3_images = get_set_images(SET3_STR)
    set4_images = get_set_images(SET4_STR)
    set5_images = get_set_images(SET5_STR)
    set6_images = get_set_images(SET6_STR)
    set7_images = get_set_images(SET7_STR)
    set8_images = get_set_images(SET8_STR)
    set9_images = get_set_images(SET9_STR)
    set10_images = get_set_images(SET10_STR)

    # ------------------------------3.2 Create JSON from Set Folders-----------------------------------
    print "3.2 Create JSON from Set Folders"
    """      Actual SET_JSON FORMAT
       set1_json = {
        "status": True,
        # "title": "",
        "folder": "set_1",
        "child_images": set1_images,
        "bucket": BUCKET_PROD
        # "set_image_url": set_images[0]
    }
    set
    """

    set1_json = {
        "status": True,
        "title": " Gallery 1",
        "folder": "set_1",
        "child_images": set1_images,
        "bucket": BUCKET_PROD
        # "set_image_url": set_images[0]
    }
    set2_json = {
        "status": True,
        "title": " Gallery 2",
        "folder": "set_2",
        "child_images": set2_images,
        "bucket": BUCKET_PROD,
    }
    set3_json = {
        "status": True,
        "title": " Gallery 3",
        "folder": "set_3",
        "child_images": set3_images,
        "bucket": BUCKET_PROD,
    }
    set4_json = {
        "status": True,
        "title": " Gallery 4",
        "folder": "set_4",
        "child_images": set4_images,
        "bucket": BUCKET_PROD,
    }
    set5_json = {
        "status": True,
        "title": " Gallery 5",
        "folder": "set_5",
        "child_images": set5_images,
        "bucket": BUCKET_PROD
    }
    set6_json = {
        "status": True,
        "title": " Gallery 6",
        "folder": "set_6",
        "child_images": set6_images,
        "bucket": BUCKET_PROD
    }
    set7_json = {
        "status": True,
        "title": " Gallery 7",
        "folder": "set_7",
        "child_images": set7_images,
        "bucket": BUCKET_PROD,
    }
    set8_json = {
        "status": True,
        "title": " Gallery 8",
        "folder": "set_8",
        "child_images": set8_images,
        "bucket": BUCKET_PROD
    }
    set9_json = {
        "status": True,
        "title": " Gallery 9",
        "folder": "set_9",
        "child_images": set9_images,
        "bucket": BUCKET_PROD
    }
    set10_json = {
        "status": True,
        "title": " Gallery 10",
        "folder": "set_10",
        "child_images": set10_images,
        "bucket": BUCKET_PROD
    }

    images = [
        set1_json, set2_json, set3_json, set4_json,
        set5_json, set6_json, set7_json, set8_json,
        set9_json, set10_json
    ]

    prod_images = {"prod_images": images}
    firebase_put(app_json, data=prod_images)

# --------------------------------------3.Make Set Title Images - btnMakeSetTitleImages-----------------------------------------
def set_title_images(app_json):
    logger_andromeda(app_json, clear=True)
    # Get set image URLs
    global APP_DIR
    APP_DIR = os.path.join(ROBOT_DIR, IN_PROCES_DIR, app_json['app_name'])
    set_items = os.listdir(os.path.join(APP_DIR, SET_IMAGE_FOLDER))
    BUKET_PROD = BUCKET_BASE + app_json['app_name'] + "/"
    # Rename the set Images --> set_1.jpg -- set_10.jpg
    set_images = []
    count = 1
    for set_item in sorted(set_items):
        if is_image(set_item):
            new_name = "set_" +str(count) + "."+ str(set_item).split(".")[-1]
            OLD_NAME = os.path.join(APP_DIR, SET_IMAGE_FOLDER, set_item)
            DEST_DIR = os.path.join(APP_DIR, SET_IMAGE_FOLDER, new_name)
            # Dont replace existing file
            if not os.path.isfile(DEST_DIR):
                shutil.move(OLD_NAME, DEST_DIR)
            set_url = BUKET_PROD + SET_IMAGE_FOLDER + "/" + new_name
            set_images.append(set_url)
            count += 1
    document_url = COUCH_DB + str(app_json['app_name']).strip()
    old_data = requests.get(document_url) #--Read the document first
    if old_data.status_code == 200:

        data_current = json.loads(old_data.content)
        if 'prod_images' in data_current and len(set_images) >= 10:
            data_current['prod_images'][0]['set_image_url'] = set_images[0]
            data_current['prod_images'][1]['set_image_url'] = set_images[1]
            data_current['prod_images'][2]['set_image_url'] = set_images[2]
            data_current['prod_images'][3]['set_image_url'] = set_images[3]
            data_current['prod_images'][4]['set_image_url'] = set_images[4]
            data_current['prod_images'][5]['set_image_url'] = set_images[5]
            data_current['prod_images'][6]['set_image_url'] = set_images[6]
            data_current['prod_images'][7]['set_image_url'] = set_images[7]
            data_current['prod_images'][8]['set_image_url'] = set_images[8]
            data_current['prod_images'][9]['set_image_url'] = set_images[9]

            firebase_put(app_json, data=data_current)
        else:
            app_json['message'] = "Please run Make Sets first. Then run make set Title Images. or Make sure you have images in 08_set_images folder."
            logger_andromeda(app_json, clear=False)
    else:
        app_json['message'] = "Status code: {0}, Error Message:{1}".format(old_data.status_code, old_data.content)
        logger_andromeda(app_json, clear=False)

    # firebase_patch(app_json, key="prod_images/0", data={"set_image_url": set_images[0]})
    # firebase_patch(app_json, key="prod_images/1", data={"set_image_url": set_images[1]})
    # firebase_patch(app_json, key="prod_images/2", data={"set_image_url": set_images[2]})
    # firebase_patch(app_json, key="prod_images/3", data={"set_image_url": set_images[3]})
    # firebase_patch(app_json, key="prod_images/4", data={"set_image_url": set_images[4]})
    # firebase_patch(app_json, key="prod_images/5", data={"set_image_url": set_images[5]})
    # firebase_patch(app_json, key="prod_images/6", data={"set_image_url": set_images[6]})
    # firebase_patch(app_json, key="prod_images/7", data={"set_image_url": set_images[7]})
    # firebase_patch(app_json, key="prod_images/8", data={"set_image_url": set_images[8]})
    # firebase_patch(app_json, key="prod_images/9", data={"set_image_url": set_images[9]})

# --------------------------------------3.Pre Publish Setup-----------------------------------------
def pre_publish_setup(app_json):
    logger_andromeda(app_json, clear=True)
    modes = {
        "image_mode": "hold",
        "admob_mode": "hold"
    }
    firebase_put(app_json, data=modes)

# --------------------------------------4.TEST JSON-----------------------------------------
def test_json(app_json):
    logger_andromeda(app_json, clear=True)
    app_json['message'] = "---------------TEST JSON BEGINS----------------------"
    logger_andromeda(app_json, clear=False)

    app_url = FIREBASE_DB + app_json['app_name']  + ".json"
    app_url = COUCH_DB + app_json['app_name']
    if validate_url(app_json, app_url) is False:
        return

    app_data_url = COUCH_DB + app_json['app_name']
    app_data = json.loads(requests.get(app_data_url).content)

    if "test_images" not in app_data:
        app_json['message'] = "test_images not found. Please run Make Set Add base JSON."
        logger_andromeda(app_json, clear=False)

    if "admob_details" not in app_data:
        app_json['message'] = "admob_details not found. Please run Maintain App Data."
        logger_andromeda(app_json, clear=False)

    prod_images_url = FIREBASE_DB + app_json['app_name'] + "/" + "prod_images.json"
    if "prod_images" in app_data:
        prod_images = app_data['prod_images']
        for image_item in prod_images:
            if "set_image_url" not in image_item:
                app_json['message'] = "set_image_url not found. Please run Make Set Title Images."
                logger_andromeda(app_json, clear=False)
            else:
                if "child_images" in image_item:
                    for img in image_item["child_images"]:
                        child_img_url = image_item["bucket"] + image_item["folder"] + "/" + img
                        validate_url(app_json, child_img_url)
                else:
                    app_json['message'] = "child_images not found. Please run Make_sets"
                    logger_andromeda(app_json, clear=False)
    else:
        app_json['message'] = "child_images not found. Please run Make_sets"
        logger_andromeda(app_json, clear=False)

    app_json['message'] = "---------------TEST JSON ENDS----------------------"
    logger_andromeda(app_json, clear=False)


# --------------------------------------3.Post Publish Setup-----------------------------------------
def post_publish_setup(app_json):
    logger_andromeda(app_json, clear=True)
    modes = {
        "image_mode": "prod",
        "admob_mode": "prod"
    }
    firebase_put(app_json, data=modes)



"""
--------------------move_valid_images_to_set----------------------------------------------
move_valid_images_to_set(item_count, set_dir)
checks whether the file is image and returns True or False
------------------------------------------------------------------------------------------
"""


def move_valid_images_to_set(item_count, set_dir):
    file_count = 0
    for item in os.listdir(os.path.join( APP_DIR, VALID_DIR_STRING)):
        if ".jpg" in item:
            if file_count >= item_count:
                break
            file_count = file_count + 1
            source = os.path.join( APP_DIR, VALID_DIR_STRING,item)
            dest = os.path.join( APP_DIR, set_dir,item)
            shutil.move(source, dest)
    return
"""
 --------------------------------APPMKRROBOT01---------------------------------------------------
 Creates Set Folders
 Distributes the images to set folders
 Creates JSON
 Uploads JSON to Firebase database
 Automatically creates JSON for all the apps in loop
 ------------------------------------------------------------------------------------------------
 --------------------JSON Structure-----------
       {
            "status": true, 
            "folder": "set_1", 
            "child_images": [
            ],
            "bucket": "https://storage.googleapis.com/robot_01/robot_test_images02/", 
            "set_image": "set_1.jpg"
        }, 
"""
# ************************************MAIN PROCESSING LOOP****************************************
# ---------------------------------3.1 Copy test JSON ------------------------------------------

app_base_json_path = os.path.join(CURR_DIR, "app_base.json")
json_file = open(app_base_json_path, "r")
json_app = json.loads(json_file.read())
json_file.close()
json_app['app_name'] = APP_NAME.replace("_", " ").title()

# Change the current dir
# os.chdir(os.path.join(ROBOT_DIR))
#
# wb = load_workbook(filename='app_database.xlsx')
# ws = wb['Sheet1']

# ---------------------------validate_app_data--------------------------------------------------
def validate_app_data(stage_dir, app_name_curr):
    error_messages = []

    # ----1. The App should be defined in app_database.xlsx
    global wb, ws
    for row in ws.iter_rows(range_string="A2:XFD1000"):
        # Set the APP details in JSON
        APP_NAME = row[1].value
        # --------------------------Exit Loop if the cell is app name is blank
        if APP_NAME is None:
            error_messages.append(
                {
                    "message":"App Not defined in app_database.xlsx",
                    "desc": "Please enter all the fields in for app in app_database.xlsx"
                }
            )
            break

        if str(APP_NAME).strip() == str(app_name_curr).strip():
            break
        # Close the workbook
    wb.close()

    #--------2.Check for Set Director

    if not os.path.isdir(os.path.join(ROBOT_DIR, stage_dir, app_name_curr , SET_IMAGE_FOLDER)):
        error_messages.append(
            {
                "message": "08_set_images Direcotry Not found.",
                "desc": "Please go back to the Queue and click on validate. It will create the folder."
            }
        )
    else:
        set_items_len = len(os.listdir(os.path.join(ROBOT_DIR, stage_dir, app_name_curr, SET_IMAGE_FOLDER)))
        if set_items_len < 10:
            error_messages.append(
                {
                    "message": "08_set_images Direcotry doesn't contain the set images properly.",
                    "desc": "Please check and correct."
                }
            )

    if len(error_messages) == 0:
        error_messages.append(
            {
                "message": "No Errors Found",
                "desc": "You can Create JSON and Upload to Cloud"
            }
        )
    return error_messages




def create_json(stage_dir, app_name_curr, mode):
    global APP_DIR
    for row in ws.iter_rows(range_string="A2:XFD1000"):
        # Set the APP details in JSON
        APP_NAME = row[1].value
        json_app['app_name'] = str(APP_NAME).replace("_", " ").title()
        json_app['admob_mode'] = mode
        json_app['image_mode'] = mode
        json_app['app_mode'] = mode
        # --------------------------Exit Loop if the cell is app name is blank
        if APP_NAME is None:
            break
        # ----------Go to next row if the Create JSON is False
        if APP_NAME <> app_name_curr: #Create JSON
            continue
        if APP_NAME == app_name_curr:  # Create JSON
            print "---------Pocessing App:", APP_NAME,"----------------------------------"
            # -----------------------------Set The App Dir
            APP_DIR = os.path.join(ROBOT_DIR, stage_dir, APP_NAME)
            # ------------------------------------1.Create Set Dir------------------------------------------
            print "1.Create Set Dir for 10 sets"
            create_dir(SET1_STR)
            create_dir(SET2_STR)
            create_dir(SET3_STR)
            create_dir(SET4_STR)
            create_dir(SET5_STR)
            create_dir(SET6_STR)
            create_dir(SET7_STR)
            create_dir(SET8_STR)
            create_dir(SET9_STR)
            create_dir(SET10_STR)
            print "1.1 Creating App Directories"
            create_dir(IN_APP_GRAPHICS)
            create_dir(SCREEN_SHOTS)
            create_dir(APK_KEY)

            valid_images_list = os.listdir(os.path.join(APP_DIR, VALID_DIR_STRING))

            total_images = len(valid_images_list)
            # Min 10 Images should be present in the APP as there are 10 sets
            if total_images<10:
                print "Min 10 Images Should be present in App: {0}".format(os.path.join( APP_DIR, VALID_DIR_STRING))
                # raise "Min 10 Images Should be present in App"
            # -----------------------------------2.Distribute valid images to sets-------------------------------
            print "2.Distribute valid images to sets"
            images_per_set = round(total_images/10.0)
            move_valid_images_to_set(images_per_set, SET1_STR)
            move_valid_images_to_set(images_per_set, SET2_STR)
            move_valid_images_to_set(images_per_set, SET3_STR)
            move_valid_images_to_set(images_per_set, SET4_STR)
            move_valid_images_to_set(images_per_set, SET5_STR)
            move_valid_images_to_set(images_per_set, SET6_STR)
            move_valid_images_to_set(images_per_set, SET7_STR)
            move_valid_images_to_set(images_per_set, SET8_STR)
            move_valid_images_to_set(images_per_set, SET9_STR)
            move_valid_images_to_set(images_per_set, SET10_STR)

            # ---------------------------------3. Create JSON for app ------------------------------------------
            print "3. Create JSON for app"
            set1_images = get_set_images(SET1_STR)
            set2_images = get_set_images(SET2_STR)
            set3_images = get_set_images(SET3_STR)
            set4_images = get_set_images(SET4_STR)
            set5_images = get_set_images(SET5_STR)
            set6_images = get_set_images(SET6_STR)
            set7_images = get_set_images(SET7_STR)
            set8_images = get_set_images(SET8_STR)
            set9_images = get_set_images(SET9_STR)
            set10_images = get_set_images(SET10_STR)
            BUKET_PROD = BUCKET_BASE+APP_NAME+"/"
            # Get set image URLs
            set_items =os.listdir(os.path.join(APP_DIR, SET_IMAGE_FOLDER))

            set_images = []
            for set_item in sorted(set_items):
                if is_image(set_item):
                    set_url = BUKET_PROD + SET_IMAGE_FOLDER + "/" + set_item
                    set_images.append(set_url)

            # ------------------------------3.2 Create JSON from Set Folders-----------------------------------
            print "3.2 Create JSON from Set Folders"
            set1_json = {
                "status": True,
                "title": str(row[20].value).strip(),
                "folder": "set_1",
                "child_images": set1_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[0]
            }
            set2_json = {
                "status": True,
                "title": str(row[21].value).strip(),
                "folder": "set_2",
                "child_images": set2_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[1]
            }
            set3_json = {
                "status": True,
                "title": str(row[22].value).strip(),
                "folder": "set_3",
                "child_images": set3_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[2]
            }
            set4_json = {
                "status": True,
                "title": str(row[23].value).strip(),
                "folder": "set_4",
                "child_images": set4_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[3]
            }
            set5_json = {
                "status": True,
                "title": str(row[24].value).strip(),
                "folder": "set_5",
                "child_images": set5_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[4]
            }
            set6_json = {
                "status": True,
                "title": str(row[25].value).strip(),
                "folder": "set_6",
                "child_images": set6_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[5]
            }
            set7_json = {
                "status": True,
                "title": str(row[26].value).strip(),
                "folder": "set_7",
                "child_images": set7_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[6]
            }
            set8_json = {
                "status": True,
                "title": str(row[27].value).strip(),
                "folder": "set_8",
                "child_images": set8_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[7]
            }
            set9_json = {
                "status": True,
                "title": str(row[28].value).strip(),
                "folder": "set_9",
                "child_images": set9_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[8]
            }
            set10_json = {
                "status": True,
                "title": str(row[29].value).strip(),
                "folder": "set_10",
                "child_images": set10_images,
                "bucket": BUKET_PROD,
                "set_image_url": set_images[9]
            }
            json_app["prod_images"] =[
                set1_json,set2_json,set3_json,set4_json,
                set5_json,set6_json,set7_json, set8_json,
                set9_json,set10_json

            ]


            out_file_name = APP_NAME.lower().replace(" ","_") + ".json"
            # ----------------------------3.25 Load Ad Ids from excel---------------------------------
            json_app["admob_details"] = {
                "banner": [row[9].value],
                "interstitial": [
                    str(row[10].value).strip(),
                    str(row[11].value).strip(),
                    str(row[12].value).strip(),
                    str(row[13].value).strip(),
                    str(row[14].value).strip(),
                    str(row[15].value).strip(),
                    str(row[16].value).strip(),
                    str(row[17].value).strip(),
                    str(row[18].value).strip(),
                    str(row[19].value).strip()
                ]

            }

            # ----------------------------3.3 Create the Description text file-----------------------
            print "3.3 Write the JSON to Google Drive and locally"
            desc_file = "App-Desc_" + APP_NAME.lower().replace(" ", "_") + ".txt"
            if not os.path.exists(os.path.join(APP_DIR,desc_file)):
                out_file = open(os.path.join(APP_DIR,desc_file), "w")
                out_file.write("Sample Desc")
                out_file.close()
            # ----------------------------3.4 Write the JSON to One Drive and locally-----------------------
            print "3.3 Write the JSON to Google Drive and locally"
            out_file = open(os.path.join(APP_DIR,out_file_name), "w")
            out_file.write(json.dumps(json_app, indent=4))
            out_file.close()
            # ----------------------------4.Upload JSON to Firebase throug REST API-----------------------------
            print "4.Upload JSON to Firebase throug REST API"

            # Create your header as required
            FIREBASE_DB = "https://robot01-a668c.firebaseio.com/"


            headers = {"content-type": "application/json", "Authorization": "<auth-key>" }

            # -----------------Create entry in app_database--------------------------
            # The app JSON key can't have '.' so replacing it by _
            if row[8].value is None:
                raise "Package name Emtpy!"
            package = str(row[8].value).replace(".", "_")

            # ----------------------------3.3 Write the JSON to Google Drive and locally-----------------------
            print "3.4 Write the JSON Link to file"
            REST_API = FIREBASE_DB + package + ".json"
            out_file = open(os.path.join(APP_DIR,"JSON_Link.txt"), "w")
            out_file.write(REST_API)
            out_file.close()
            # ----------------------------4.Upload JSON to Firebase through REST API-----------------------------

            if True is True:
                # save JSON with app name
                REST_API = FIREBASE_DB + out_file_name
                r_JSON_app_name = requests.put(REST_API, data=json.dumps(json_app), headers=headers)
                # save JSON with package name
                REST_API2 = FIREBASE_DB + package + ".json"
                r_JSON_package_name = requests.put(REST_API2, data=json.dumps(json_app), headers=headers)
                # save {package name-->appname}
                print "3.31 Create Entry in App Data Base {package name-->appname}"
                FIREBASE_APP_DB = FIREBASE_DB + "app_database" + "/" + package + ".json"
                r_package_app = requests.put(FIREBASE_APP_DB, data=json.dumps(out_file_name), headers=headers)
                # save {appname-->packagename}
                FIREBASE_APP_DB2 = FIREBASE_DB + "app_database" + "/" + out_file_name
                print "3.31 Create Entry in App Data Base {appname-->packagename}"
                r_app_package = requests.put(FIREBASE_APP_DB2, data=json.dumps(package.replace(".json", "")), headers=headers)

                print "API Put request status: {0} : {1} : {2}:{3}".format(
                    r_JSON_app_name.status_code , r_JSON_package_name.status_code, r_package_app.status_code,
                r_app_package.status_code)
                if r_JSON_app_name.status_code <> 200 or r_JSON_package_name.status_code <> 200 \
                        or r_package_app.status_code <> 200 or r_app_package.status_code <> 200:
                    raise "JSON SAVING FAILED"
                else:
                    print "JSON:"+str(out_file_name) + " Saved successfully."
                    print REST_API2
                    return "Success:", REST_API2
            else:
                print "JSON saved to folder not uploaded to Firebase."
                return "Fail"
    wb.close()

# create_json("InProcess",'bridal_mehndi_designs_for_hands',"test")
# ---------------------------------UPLOAD TO GCLOUD----------------------------------------------
def upload_gcloud(app_json):
    # gsutil
    # cp - r
    # indian_mehndi_designs
    # gs: // robot_01 / indian_mehndi_designs
    logger_andromeda(app_json, clear=True)
    LOCAL_DIR = os.path.join(ROBOT_DIR, app_json['stage_dir'], app_json['app_name'])
    LOCAL_DIR = LOCAL_DIR.replace("//", "\\")
    BUCKET_BASE = "gs://robot_01/"
    BUCKET_DIR = BUCKET_BASE + app_json['app_name']
    LOG_FILE_PATH = os.path.join(LOCAL_DIR, LOG_DIR, str(app_json['action']).strip() + "_log.txt")
    gcloud_upload_cmd = ["gsutil", "cp", "-r", LOCAL_DIR, BUCKET_DIR, ">", LOG_FILE_PATH, "2>&1"]
    print gcloud_upload_cmd
    gcloud_upload_process = subprocess.Popen(gcloud_upload_cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)

    gcloud_upload_process.communicate(b"input data that is passed to subprocess' stdin")

    f = open(LOG_FILE_PATH)
    gcloud_log = str(f.read())
    f.close()
    app_json['message'] = "Uploaded App:{0} To {1}".format(app_json['app_name'], BUCKET_DIR) + "\n" + gcloud_log
    #
    # if "Copying" in gcloud_log:
    #     app_json['message'] = "Uploaded App:{0} To {1}".format(app_json['app_name'], BUCKET_DIR) + "\n" + gcloud_log
    # else:
    #     app_json['message'] = APP_NAME + " Upload FAILED" + "\n" + err
    logger_andromeda(app_json, clear=False)
    print gcloud_log

# ---------------------------------UPLOAD TO Minio----------------------------------------------
def btnUploadtoMinio(app_json):
    # mc
    # cp - r
    # indian_mehndi_designs
    # minio1/test / indian_mehndi_designs
    logger_andromeda(app_json, clear=True)
    LOCAL_DIR = os.path.join(ROBOT_DIR, app_json['stage_dir'], app_json['app_name'])
    LOCAL_DIR = LOCAL_DIR.replace("//", "\\")
    LOG_FILE_PATH = os.path.join(LOCAL_DIR, LOG_DIR, str(app_json['action']).strip() + "_log.txt")
    gcloud_upload_cmd = ["mc", "cp", "-r", LOCAL_DIR, MINIO_BUCKET, ">", LOG_FILE_PATH, "2>&1"]
    print gcloud_upload_cmd
    gcloud_upload_process = subprocess.Popen(gcloud_upload_cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)

    gcloud_upload_process.communicate(b"input data that is passed to subprocess' stdin")

    f = open(LOG_FILE_PATH)
    gcloud_log = str(f.read())
    f.close()
    app_json['message'] = "MINIO: Uploaded App:{0} To {1}".format(app_json['app_name'], MINIO_BUCKET) + "\n" + gcloud_log
    #
    # if "Copying" in gcloud_log:
    #     app_json['message'] = "Uploaded App:{0} To {1}".format(app_json['app_name'], BUCKET_DIR) + "\n" + gcloud_log
    # else:
    #     app_json['message'] = APP_NAME + " Upload FAILED" + "\n" + err
    logger_andromeda(app_json, clear=False)
    print gcloud_log


# ---------------------------------DELETE TO GCLOUD----------------------------------------------
def delete_gcloud(app_json):
    # gsutil
    # rm -r
    # gs: // robot_01 / indian_mehndi_designs
    logger_andromeda(app_json, clear=True)
    LOCAL_DIR = os.path.join(ROBOT_DIR, app_json['stage_dir'], app_json['app_name'])
    LOCAL_DIR = LOCAL_DIR.replace("//", "\\")
    BUCKET_BASE = "gs://robot_01/"
    BUCKET_DIR = BUCKET_BASE + app_json['app_name']
    LOG_FILE_PATH = os.path.join(LOCAL_DIR, LOG_DIR, str(app_json['action']).strip() + "_log.txt")
    gcloud_upload_cmd = ["gsutil", "rm", "-r", BUCKET_DIR, ">", LOG_FILE_PATH , "2>&1"]
    print gcloud_upload_cmd
    gcloud_delete_process = subprocess.Popen(gcloud_upload_cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE,shell=True)
    log, err = gcloud_delete_process.communicate()

    f = open(LOG_FILE_PATH)
    gcloud_log = str(f.read())
    f.close()
    app_json['message'] = "Deleted App:{0} from {1}".format(app_json['app_name'], BUCKET_DIR) + "\n" + gcloud_log
    #
    # if " " in gcloud_log:
    #     app_json['message'] = "Uploaded App:{0} To {1}".format(app_json['app_name'], BUCKET_DIR) + "\n" + gcloud_log
    # else:
    #     app_json['message'] = APP_NAME + " Upload FAILED" + "\n" + str(err)
    logger_andromeda(app_json, clear=False)

    print gcloud_log

# ---------------------------------DELETE FROM MINIO----------------------------------------------
def btnDelFromMinio(app_json):
    # mc
    # rm -r
    # minio/test / indian_mehndi_designs
    logger_andromeda(app_json, clear=True)
    LOCAL_DIR = os.path.join(ROBOT_DIR, app_json['stage_dir'], app_json['app_name'])
    LOCAL_DIR = LOCAL_DIR.replace("//", "\\")
    BUCKET_DIR = MINIO_BUCKET + app_json['app_name']
    LOG_FILE_PATH = os.path.join(LOCAL_DIR, LOG_DIR, str(app_json['action']).strip() + "_log.txt")
    gcloud_upload_cmd = ["mc", "rm", "-r", "--force" , BUCKET_DIR, ">", LOG_FILE_PATH , "2>&1"]
    print gcloud_upload_cmd
    gcloud_delete_process = subprocess.Popen(gcloud_upload_cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE,shell=True)
    log, err = gcloud_delete_process.communicate()

    f = open(LOG_FILE_PATH)
    gcloud_log = str(f.read())
    f.close()
    app_json['message'] = "Deleted App:{0} from {1}".format(app_json['app_name'], BUCKET_DIR) + "\n" + gcloud_log
    #
    # if " " in gcloud_log:
    #     app_json['message'] = "Uploaded App:{0} To {1}".format(app_json['app_name'], BUCKET_DIR) + "\n" + gcloud_log
    # else:
    #     app_json['message'] = APP_NAME + " Upload FAILED" + "\n" + str(err)
    logger_andromeda(app_json, clear=False)

    print gcloud_log


# upload_gcloud("", "")