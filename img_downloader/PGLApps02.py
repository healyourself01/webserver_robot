import os, requests, json, urllib2, time, urllib3
import APP_CONFIG as CG
from socket import error

# Search the string and print the line if it matches
def search_str(file,srch_str):
    file = str(file).replace("\\", "//")
    input_file = open(file, 'r')
    count_lines = 0
    for line in input_file:
        if srch_str in line:
            print ('------',file,'--------')
            print (line)
        count_lines += 1
#     print ('number of lines:', count_lines)

PREV_RETURN_CODE =True
ERROR_COUNT = 0

def set_err_count(CURR_RETURN_CODE):
    global ERROR_COUNT, PREV_RETURN_CODE
    if (CURR_RETURN_CODE is False) and (CURR_RETURN_CODE == PREV_RETURN_CODE):
        ERROR_COUNT += 1
    if (CURR_RETURN_CODE is False) and (CURR_RETURN_CODE <> PREV_RETURN_CODE):
        ERROR_COUNT = 0
    PREV_RETURN_CODE = CURR_RETURN_CODE


def download_img_from_url(img_url, dir_path, img_name):
        global PREV_RETURN_CODE, CURR_RETURN_CODE
        if not os.path.isfile(os.path.join(dir_path, img_name)):
            try:
                req = urllib2.Request(img_url, headers={'User-Agent': CG.HEADERS_SCRAPER})
                print ("Downloading Image:",dir_path, img_name)
                raw_img = urllib2.urlopen(req).read()
                File = open(os.path.join(dir_path, img_name), "wb")
                File.write(raw_img)
                File.close()
                time.sleep(0.2)  # Give 1 sec Break
                PREV_RETURN_CODE = True
                return True
            except urllib2.HTTPError as e:
                if e.code == 404:
                    print ("******404 Downloading Image:", img_url)
                    set_err_count(False)
                    return False

                if e.code == 502: #Bad gateway
                    print ("******502 Downloading Image:",img_url)
                    raw_img = urllib2.urlopen(req).read()
                    File = open(os.path.join(dir_path, img_name), "wb")
                    File.write(raw_img)
                    File.close()
                    time.sleep(0.2)  # Give 1 sec Break
                    PREV_RETURN_CODE =True
                    return True
                print ("******Failed to Downloading Image:", img_url)
            except error:
                print ("******Socket error:", img_url)
                # Wait for 10 sec when socket error
                time.sleep(10)
            finally:
                set_err_count(False)
                return False

        else:
            print "Image Already present:", os.path.join(dir_path, img_name)
            PREV_RETURN_CODE = True
            return True


# rangoli_url = "http://lagadpankaj1992.890m.com/festival_rangoli_mehndi/json_rangoli.json"
# items = json.loads(requests.get(rangoli_url).content)
#
# OUT_RANGOLI = "D://Andomeda Robot//PGLApps//Rangoli"
#
# for item in items['heroes']:
#     print (item['url_address'])
#     err_count = 0
#     for count in range(1,30000):
#         img_url = item['url_address'] + "("+ str(count)+").jpg"
#         img_name = str(item["actress_name"]).replace(" ","_") +"_" + str(count) + ".jpg"
#         result = download_img_from_url(img_url=img_url,dir_path=OUT_RANGOLI,img_name=img_name)
#         if ERROR_COUNT >= 20:
#             ERROR_COUNT = 0
#             print ('-END------------------', item['url_address'], '-------------------------')
#             break
#
mehndi_url = "http://lagadpankaj1992.890m.com/festival_rangoli_mehndi/json_mehndi.json"
items = json.loads(requests.get(mehndi_url).content)

OUT_MEHNDI = "D://Andomeda Robot//PGLApps//Mehndi"

for item in items['heroes']:
    print ('-BEGIN------------------', item['url_address'], '-------------------------')
    err_count = 0
    for count in range(1,30000):
        img_url = item['url_address'] + "(" + str(count) + ").jpg"
        img_name = str(item["actress_name"]).replace(" ","_") +"_" + str(count) + ".jpg"
        result = download_img_from_url(img_url=img_url,dir_path=OUT_MEHNDI,img_name=img_name)
        if ERROR_COUNT >= 20:
            ERROR_COUNT = 0
            print ('-END------------------', item['url_address'], '-------------------------')
            break
