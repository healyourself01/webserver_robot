
  var Socket;
  var images = [];
  var imgname = "";
  var radioValue = "";
  var host_name = "";




  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};



      function fnWebSocket()
         {
            var websocket_url = "WS://"+ host_name.replace("http://", "") + "/" + "image_validation_ws";
            if ("WebSocket" in window)
            {
               console.log("WebSocket is supported by your Browser!");

               // Let us open a web socket
               Socket = new WebSocket(websocket_url);

               Socket.onopen = function()
               {
                  // Web Socket is connected, send data using send()
                  console.log("socket opened");

               };

               Socket.onmessage = function (evt)
               {
                  var received_msg = evt.data;
                  if (received_msg == "success"){
                  location.reload();
                  }
               };

               Socket.onclose = function()
               {
                  // websocket is closed.
          console.log("socket closed");
                 };
            }

            else
            {
               // The browser doesn't support WebSocket
               alert("WebSocket NOT supported by your Browser!");
            }
         }

$(document).ready(function(){
    //Set the host name global variable
    host_name = $(location).attr('host')  ;

fnWebSocket();

    $("#submit").click(function(){
        console.log("Submit clicked");
//Write the App Name First
images.push(
{ "app_name" :getUrlParameter("app_name"),
   "origin":"validation"});
        //processing this row
        //how to process each cell(table td) where there is checkbox
        $('td').each(function (i, cell) {
            var $cell = $(cell);
            imgname = "";
            radioValue = "";

            imgname = $cell.find('img').attr("src");
            radioValue =$cell.find("input[name='validation']:checked").val();
            images.push (
            
            {
                "item": imgname,
                "option": radioValue
            }
            
            );
            
        });

        
//    });

    ;
        Socket.send(JSON.stringify(images));
          console.log(images);
//        Reload the page after sending the JSON
//    location.reload();
    });

});

/*Loops Thrugh the items in the HTML and */

