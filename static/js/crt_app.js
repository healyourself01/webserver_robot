
  var Socket;
  var images = [];
  var imgname = "";
  var radioValue = "";
  var QUEUE_DIR = "Queue";
  var LOG_DIR = "10_Logs";
  var host_name = "";

  //-------------------------------------------Validate Package Name------------------------------------------
 function fnValidatePackage(package) {
     var string = "foo",
    expr = "com_/";
    string.match(expr);

    if (package.indexOf(".") != -1){
        alert("Package Name can't contain '.'!!")
        return false;
    }
    if (package.indexOf("com_mindgame") != 0){
        alert("Package Name must start with com_mindgame!!")
        return false;
    }

    //Valid input
     return true;
     $("#olLogger").append("Valid Package Name.");

 }



//--------------------------------------GET URL PARAMS-----------------------------------
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

//----------------------------------------Initialize Active job log----------------------------
function fnInitializeLogger(jobname){
  $("#olLogger").html("");
  $('#JobName').text("Job:" + jobname);
}

//----------------------------------------Initialize Job log Viewer-----------------------------
function fnInitializeViewer(jobname){
    //  Clear the old log first
  $("#txtJobViewer").html("");
  //Load the New log

  url_log_file = "http://" + host_name + "/static/"  + "production_robot_apps/" +
  QUEUE_DIR + "/" + getUrlParameter("app_name") + "/" + LOG_DIR + "/" + jobname
                        + ".txt";
  console.log(url_log_file);
        $.ajax({
           url : url_log_file,
           dataType: "text",
           success : function (data) {
               $("#txtJobViewer").text(data);
           }
       });
  $('#JobName2').text("Job:" + jobname);
}

//------------------------------------ WEB SOCKET----------------------------------------------------------
      function fnWebSocket()
         {
            var websocket_url = "WS://"+ host_name.replace("http://", "") + "/" + "queue_websocket";
            console.log(websocket_url)
            if ("WebSocket" in window)
            {
               console.log("WebSocket is supported by your Browser!");

               // Let us open a web socket
               Socket = new WebSocket(websocket_url);

               Socket.onopen = function()
               {
                  // Web Socket is connected, send data using send()
                  console.log("socket opened");

               };

               Socket.onmessage = function (evt)
               {
                  var received_msg = evt.data;

//                  alert(received_msg);
                     var item1 = $("<li></li>").text(received_msg);
                      $("#olLogger").append(item1);

               };

               Socket.onclose = function()
               {
                  // websocket is closed.
          console.log("socket closed");
          fnWebSocket();
                 };
            }

            else
            {
               // The browser doesn't support WebSocket
               alert("WebSocket NOT supported by your Browser!");
            }
         }
//--------------------------------------MAIN FUNCTION--------------------------------------------------------
$(document).ready(function(){
    //Set the host name global variable
    host_name = $(location).attr('host')  ;

    fnWebSocket();

//-------------------------------------SUBMIT FORM----------------------------------------------------------

    $("#btnCreateApp").click(function(){
        console.log("#btnCreateApp clicked");
            fnInitializeLogger("btnCreateApp");

            if  ( fnValidatePackage($.trim( $('#inPackageName').val()) ) ) {


//Prepare the App Data and send to server
                app_data = {
                    "origin": "queue-app",
                    "action": "btnCreateApp",
                    "stage_dir": "Queue",
                    "app_name": $.trim($('#inPackageName').val()),
                    "package": $.trim($('#inPackageName').val())
                }

                Socket.send(JSON.stringify(app_data));
                console.log(app_data);
            }
    });


});

/*Loops Thrugh the items in the HTML and */

