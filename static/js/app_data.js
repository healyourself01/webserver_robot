
  var Socket;
  var images = [];
  var imgname = "";
  var radioValue = "";
  var QUEUE_DIR = "Queue";
  var LOG_DIR = "10_Logs";
  var host_name = "";

//--------------------------------------GET URL PARAMS-----------------------------------
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

//----------------------------------------Initialize Active job log----------------------------
function fnInitializeLogger(jobname){
  $("#olLogger").html("");
  $('#JobName').text("Job:" + jobname);
}

//----------------------------------------Initialize Job log Viewer-----------------------------
function fnInitializeViewer(jobname){
    //  Clear the old log first
  $("#txtJobViewer").html("");
  //Load the New log

  url_log_file = "http://" + host_name + "/static/"  + "production_robot_apps/" +
  QUEUE_DIR + "/" + getUrlParameter("app_name") + "/" + LOG_DIR + "/" + jobname
                        + ".txt";
  console.log(url_log_file);
        $.ajax({
           url : url_log_file,
           dataType: "text",
           success : function (data) {
               $("#txtJobViewer").text(data);
           }
       });
  $('#JobName2').text("Job:" + jobname);
}

//------------------------------------ WEB SOCKET----------------------------------------------------------
      function fnWebSocket()
         {
            var websocket_url = "WS://"+ host_name.replace("http://", "") + "/" + "processapp";
            console.log(websocket_url)
            if ("WebSocket" in window)
            {
               console.log("WebSocket is supported by your Browser!");

               // Let us open a web socket
               Socket = new WebSocket(websocket_url);

               Socket.onopen = function()
               {
                  // Web Socket is connected, send data using send()
                  console.log("socket opened");

               };

               Socket.onmessage = function (evt)
               {
                  var received_msg = evt.data;

//                  alert(received_msg);
                     var item1 = $("<li></li>").text(received_msg);
                      $("#olLogger").append(item1);

               };

               Socket.onclose = function()
               {
                  // websocket is closed.
          console.log("socket closed");
          fnWebSocket();
                 };
            }

            else
            {
               // The browser doesn't support WebSocket
               alert("WebSocket NOT supported by your Browser!");
            }
         }
//--------------------------------------MAIN FUNCTION--------------------------------------------------------
$(document).ready(function(){
    //Set the host name global variable
    host_name = $(location).attr('host')  ;

    fnWebSocket();

//-------------------------------------SUBMIT FORM----------------------------------------------------------

    $("#btnSubmit").click(function(){
        console.log("#btnSubmit clicked");
            fnInitializeLogger("btnSubmit");

//Write the App Name First
        app_data = {

            "origin":"app-data",
            "action": "btnSubmit",
            "stage_dir": "InProcess",
            "app_name":$.trim( $('#inAppName').val() ),
            "package": getUrlParameter("app_name"),
            "banner_id": $.trim( $('#inAdmobBanner').val() ),
            "inter_id1": $.trim( $('#inAdmobInt1').val() ),
            "inter_id2": $.trim( $('#inAdmobInt2').val() ),
            "inter_id3": $.trim( $('#inAdmobInt3').val() ),
            "inter_id4": $.trim( $('#inAdmobInt4').val() ),
            "desc_short": $.trim( $('#txtShortDesc').val() ),
            "desc_detailed": $.trim( $('#txtDetailedDesc').val() )
        }

        Socket.send(JSON.stringify(app_data));
          console.log(app_data);

    });


});

/*Loops Thrugh the items in the HTML and */

