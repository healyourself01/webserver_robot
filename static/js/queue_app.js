
  var Socket;
  var images = [];
  var imgname = "";
  var host_name = "";
  var radioValue = "";
  var QUEUE_DIR = "Queue";
  var LOG_DIR = "10_Logs";



  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

/*------------GET CONFIRMATION------------------------BEGIN---------------------------*/
              function getConfirmation(){
               var retVal = confirm("Do you want to continue ?");
               if( retVal == true ){
                  console.log("User wants to continue!");
                  return true;
               }
               else{
                  console.log("User does not want to continue!");
                  return false;
               }
            }
/*------------GET CONFIRMATION------------------------END----------------------------*/


//Initialize Active job log
function fnInitializeLogger(jobname){
  $("#olLogger").html("");
  $('#JobName').text("Job:" + jobname);
}

//Initialize Job log Viewer
function fnInitializeViewer(jobname){
    //  Clear the old log first
  $("#txtJobViewer").html("");
  //Load the New log

  url_log_file = "http://" + host_name + "/static/" + "production_robot_apps/" +
  QUEUE_DIR + "/" + getUrlParameter("app_name") + "/" + LOG_DIR + "/" + jobname
                        + ".txt";
  console.log(url_log_file);
        $.ajax({
           url : url_log_file,
           dataType: "text",
           success : function (data) {
               $("#txtJobViewer").text(data);
           }
       });
  $('#JobName2').text("Job:" + jobname);
}


      function fnWebSocket()
         {
             var websocket_url = "WS://"+ host_name.replace("http://", "") + "/" + "queue_websocket";
            if ("WebSocket" in window)
            {
               console.log("WebSocket is supported by your Browser!");

               // Let us open a web socket
               Socket = new WebSocket(websocket_url);

               Socket.onopen = function()
               {
                  // Web Socket is connected, send data using send()
                  console.log("socket opened");

               };

               Socket.onmessage = function (evt)
               {
                  var received_msg = evt.data;

//                  alert(received_msg);
                     var item1 = $("<li></li>").text(received_msg);
                      $("#olLogger").append(item1);

               };

               Socket.onclose = function()
               {
                  // websocket is closed.
          console.log("socket closed");
          fnWebSocket();
                 };
            }

            else
            {
               // The browser doesn't support WebSocket
               alert("WebSocket NOT supported by your Browser!");
            }
         }

$(document).ready(function(){
    //Set the host name global variable
    host_name = $(location).attr('host')  ;

fnWebSocket();

    $("#btnMovToInProgress").click(function(){


        console.log("#btnMovToInProgress clicked");
//Write the App Name First
        process_app = {
        "origin":"queue-app",
        "action": "btnMovToInProgress",
        "stage_dir": "Queue",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);



    });

    $("#btnCrtAppDir").click(function(){
        console.log("#btnMovToInProgress clicked");
            fnInitializeLogger("btnMovToInProgress");

//Write the App Name First
        process_app = {
        "origin":"queue-app",
        "action": "btnCrtAppDir",
        "stage_dir": "Queue",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);

    });


   $("#btnCrtAppDir_log").click(function(){
        console.log("#btnCrtAppDir_log clicked");
            fnInitializeViewer("btnCrtAppDir_log");

    });

 /*--------------------DELETE ALL IMAGES--------BEGIN------------------------*/
    $("#btnDelAllImages").click(function(){
        console.log("#btnDelAllImages clicked");
         result = getConfirmation();

         if (result == true) {
             process_app = {
                 "origin": "queue-app",
                 "action": "btnDelAllImages",
                 "stage_dir": "Queue",
                 "mode": "test",
                 "app_name": getUrlParameter("app_name")
             }

             Socket.send(JSON.stringify(process_app));
             console.log(process_app);
         }
    });
/*--------------------DELETE ALL IMAGES--------END-----------------------------*/
/*--------------------RENAME ALL IMAGES--------BEGIN-----------------------------*/
    $("#btnRenImages").click(function(){
        console.log("#btnRenImages clicked");

        process_app = {
        "origin":"queue-app",
        "action": "btnRenImages",
        "stage_dir": "Queue",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });
/*--------------------RENAME ALL IMAGES--------END-----------------------------*/
    $("#btnMoveAllToValid").click(function(){
        console.log("#btnRenImages clicked");

        process_app = {
        "origin":"queue-app",
        "action": "btnMoveAllToValid",
        "stage_dir": "Queue",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }
        Socket.send(JSON.stringify(process_app));
        console.log(process_app);
    });

});

/*Loops Thrugh the items in the HTML and */

