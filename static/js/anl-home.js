
  var Socket;
  var images = [];
  var imgname = "";
  var radioValue = "";
  var host_name ="";

IN_PROCESS_DIR = "InProcess"
var LOG_DIR = "10_Logs";

  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

//Initialize Active job log
function fnInitializeLogger(jobname){
  $("#olLogger").html("");
  $('#JobName').text("Job:" + jobname);
}

//Initialize Job log Viewer
function fnInitializeViewer(jobname){
    //  Clear the old log first
  $("#txtJobViewer").html("");
  //Load the New log

  url_log_file = "http://" + host_name + "/static/" + "production_robot_apps/" +
  IN_PROCESS_DIR + "/" + getUrlParameter("app_name") + "/" + LOG_DIR + "/" + jobname
                        + ".txt";
  console.log(url_log_file);
        $.ajax({
           url : url_log_file,
           dataType: "text",
           success : function (data) {
               $("#txtJobViewer").text(data);
           }
       });
  $('#JobName2').text("Job:" + jobname);
}



      function fnWebSocket()
         {
             var websocket_url = "WS://"+ host_name.replace("http://", "") + "/" + "processapp";
            if ("WebSocket" in window)
            {
               console.log("WebSocket is supported by your Browser!");

               // Let us open a web socket
               Socket = new WebSocket(websocket_url);

               Socket.onopen = function()
               {
                  // Web Socket is connected, send data using send()
                  console.log("socket opened");

               };

               Socket.onmessage = function (evt)
               {
                  var received_msg = evt.data;

                  // alert(received_msg);
                     var item1 = $("<li></li>").text(received_msg);
                      $("#olLogger").append(item1);

               };

               Socket.onclose = function()
               {
                  // websocket is closed.
          console.log("socket closed");
          fnWebSocket();
                 };
            }

            else
            {
               // The browser doesn't support WebSocket
               alert("WebSocket NOT supported by your Browser!");
            }
         }

$(document).ready(function(){
    //Set the host name global variable
    host_name = $(location).attr('host')  ;

fnWebSocket();


    //-------------------------btnSetTestMode----------------------------------------------------
    $("#btnSetTestMode").click(function(){
        console.log("#btnSetTestMode clicked");
        fnInitializeLogger("btnSetTestMode");

//Write the App Name First
        process_app = {
        "origin":"process-app",
        "action": "btnSetTestMode",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });

       $("#btnSetTestMode_log").click(function(){
        console.log("#btnSetTestMode_log clicked");
            fnInitializeViewer("btnSetTestMode_log");

    });

    //--------------------------btnAddTestImages----------------------------------------------------
    $("#btnAddBaseJSON").click(function(){
        console.log("#btnAddBaseJSON clicked");
        fnInitializeLogger("btnAddBaseJSON");

        process_app = {
        "origin":"process-app",
        "action": "btnAddBaseJSON",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });
       $("#btnAddBaseJSON_LOG").click(function(){
        console.log("#btnAddBaseJSON_LOG clicked");
            fnInitializeViewer("btnAddBaseJSON_LOG");

    });

    //--------------------------btnMakeSets----------------------------------------------------
    $("#btnMakeSets").click(function(){
        console.log("#btnMakeSets clicked");
        fnInitializeLogger("btnMakeSets");
        process_app = {
        "origin":"process-app",
        "action": "btnMakeSets",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });

       $("#btnMakeSets_log").click(function(){
        console.log("#btnMakeSets_log clicked");
            fnInitializeViewer("btnMakeSets_log");

    });
    //--------------------------btnMakeSetTitleImages----------------------------------------------------
    $("#btnMakeSetTitleImages").click(function(){
        console.log("#btnMakeSetTitleImages clicked");

        fnInitializeLogger("btnMakeSetTitleImages");

        process_app = {
        "origin":"process-app",
        "action": "btnMakeSetTitleImages",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });
     $("#btnMakeSetTitleImages_log").click(function(){
        console.log("#btnMakeSetTitleImages_log clicked");
            fnInitializeViewer("btnMakeSetTitleImages_log");

    });
    //--------------------------btnUploadtoGcloud----------------------------------------------------
    $("#btnUploadtoGcloud").click(function(){
        console.log("#btnUploadtoGcloud clicked");
        fnInitializeLogger("btnUploadtoGcloud");
        process_app = {
        "origin":"process-app",
        "action": "btnUploadtoGcloud",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });
   $("#btnUploadtoGcloud_log").click(function(){
        console.log("#btnMakeSetTitleImages_log clicked");
            fnInitializeViewer("btnUploadtoGcloud_log");
     });

    //--------------------------btnDelFromGcloud----------------------------------------------------
    $("#btnDelFromGcloud").click(function(){
        console.log("#btnDelFromGcloud clicked");
        fnInitializeLogger("btnDelFromGcloud");

        process_app = {
        "origin":"process-app",
        "action": "btnDelFromGcloud",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });
    $("#btnDelFromGcloud_log").click(function(){
        console.log("#btnDelFromGcloud_log clicked");
            fnInitializeViewer("btnDelFromGcloud_log");
     });
    //--------------------------btnTestJSON----------------------------------------------------
    $("#btnTestJSON").click(function(){
        console.log("#btnTestJSON clicked");
        fnInitializeLogger("btnTestJSON");
        process_app = {
        "origin":"process-app",
        "action": "btnTestJSON",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });
    $("#btnTestJSON_log").click(function(){
        console.log("#btnTestJSON_log clicked");
            fnInitializeViewer("btnTestJSON_log");
     });

    //--------------------------btnPrePublishSetup----------------------------------------------------
    $("#btnPrePublishSetup").click(function(){
        console.log("#btnPrePublishSetup clicked");
                fnInitializeLogger("btnPrePublishSetup");

        process_app = {
        "origin":"process-app",
        "action": "btnPrePublishSetup",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });

        $("#btnPrePublishSetup_log").click(function(){
        console.log("#btnPrePublishSetup_log clicked");
            fnInitializeViewer("btnPrePublishSetup_log");
     });

    //--------------------------btnPostPublishSetup----------------------------------------------------
    $("#btnPostPublishSetup").click(function(){
        console.log("#btnPostPublishSetup clicked");
                fnInitializeLogger("btnPostPublishSetup");

        process_app = {
        "origin":"process-app",
        "action": "btnPostPublishSetup",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });

        $("#btnPostPublishSetup_log").click(function(){
        console.log("#btnPostPublishSetup_log clicked");
            fnInitializeViewer("btnPostPublishSetup_log");
     });

    //--------------------------btnMoveToPublished----------------------------------------------------
    $("#btnMoveToPublished").click(function(){
        console.log("#btnMoveToPublished clicked");
         fnInitializeLogger("btnMoveToPublished");
        process_app = {
        "origin":"process-app",
        "action": "btnMoveToPublished",
        "stage_dir": "InProcess",
        "mode":"test",
        "app_name":getUrlParameter("app_name")
        }

        Socket.send(JSON.stringify(process_app));
          console.log(process_app);
    });
        $("#btnMoveToPublished_log").click(function(){
        console.log("#btnMoveToPublished_log clicked");
            fnInitializeViewer("btnMoveToPublished_log");
     });


});

/*Loops Thrugh the items in the HTML and */

