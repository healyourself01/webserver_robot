import urllib2
from BeautifulSoup import BeautifulSoup
from datetime import datetime
import APP_CONFIG as CG
from dateutil import relativedelta
import sqlite3, requests
import AndromedaPy as AP
# https://www.crummy.com/software/BeautifulSoup/bs4/doc/#css-selectors

# ---------------------------IS NUMBER------------------------------------------------------------
import re, unicodedata
import DataDescriptions as DD
import AndromedaPy as AP
def is_number_regex(s):
    """ Returns True is string is a number. """
    if re.match("^\d+?\.\d+?$", s) is None:
        return s.isdigit()
    return True

from selenium.common.exceptions import NoSuchElementException, WebDriverException
def check_exists_by_xpath(driver, xpath):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True


# ------------get_text_by_CSS_Selector------------------BEGIN--------------------------------------
def get_text_by_CSS_Selector(soup, css_selector):
    value = None
    value = soup.select_one(css_selector)
    if value is None:
        return None
    else:
        try:
            if is_number_regex(value.text):
                return float(AP.conv_ascii(value.text))
            else:
                return AP.conv_ascii(value.text)
        except UnicodeEncodeError:
            print "error:", css_selector
            return "error:", css_selector
# ------------get_text_by_CSS_Selector-------------------END---------------------------------------

# ------------get_app_data-------------------BEGIN---------------------------------------
def get_app_data(soup, url):
    scraped_data = {
            "_id": url.split("=")[-1],
            DD.PLAPHDR_PLAHURL_01:url,
            DD.PLAPDTL_PLADTTL_02: get_text_by_CSS_Selector(soup, ".id-app-title"),
            DD.PLAPDTL_PLADVER_07:get_text_by_CSS_Selector(soup, "div[itemprop='softwareVersion']"),
            DD.PLAPDTL_PLADDEV_06: get_text_by_CSS_Selector(soup, "span[itemprop='name']"),
            DD.PLAPDTL_PLADRCHG_11: get_text_by_CSS_Selector(soup,'.recent-change'),
            DD.PLAPDTL_PLADEML_08:str(get_text_by_CSS_Selector(soup, '.dev-link')).replace("Email ",''),
            DD.PLAPDTL_PLADSCR_10:get_text_by_CSS_Selector(soup, ".score"),
            DD.PLAPDTL_PLADRVW_09:get_text_by_CSS_Selector(soup, 'reviews-num'),
            DD.PLAPDTL_PLADDAT_06:get_text_by_CSS_Selector(soup, "div[itemprop='datePublished']"),
            DD.PLAPDTL_PLADINST_05:get_text_by_CSS_Selector(soup, "div[itemprop='numDownloads']"),
            DD.PLAPDTL_PLADDSC_12: get_text_by_CSS_Selector(soup, '.show-more-content')
        }
    return scraped_data
# ------------get_app_data-------------------END---------------------------------------

# ------------------MIN and MAX Installs --------------BEGIN----------------------------
def get_min_installs(installs):
    if installs is not None:
        array = str(installs).split("-")
        if len(array) == 2:
            return float(array[0].replace(",",""))
        else:
            return None
    else:
        return None

def get_max_installs(installs):
    if installs is not None:
        array = str(installs).split("-")
        if len(array) == 2:
            return float(array[-1].replace(",",""))
        else:
            return None
    else:
        return None
# ------------------MIN and MAX Installs --------------END-------------------------
# -----------------------Julian Date Conversion--------------------------

def conv_date_julian(date_in):
#     date = datetime.strptime('Thu, 16 Dec 2010 12:14:05', '%a, %d %b %Y %H:%M:%S')
    date_tmp = datetime.strptime(date_in, '%d %B %Y')
    date_tmp = date_tmp.strftime("%Y-%m-%d")
    con = sqlite3.connect(":memory:")
    # list(con.execute("SELECT julianday('2017-02-01')"))[0][0]
    return list(con.execute("SELECT julianday(" + date_tmp + ")"))[0][0]# --------------------------------DAYS AND MONTHS DIFF------------------------------
# -------------------DATES CONVERSION--------------------------------------------
def dates_diff_days(date_in):
    if date_in is not None:
        date1 = datetime.strptime(date_in, '%d %B %Y')
        date2 = datetime.strptime(CG.CURRENT_DATE, "%Y-%m-%d")
        delta = (date2 - date1)
        return delta.days

def dates_diff(date_in):
    if date_in is not None:
        date1 = datetime.strptime(date_in, '%d %B %Y')
        date2 = datetime.strptime(CG.CURRENT_DATE, "%Y-%m-%d")
        delta = relativedelta.relativedelta(date2, date1)
        return delta

def installs_per_day(installs, days):
    if (installs is not None) and (days is not None) and (days != 0):
        return (installs/days)
    else:
        return None

# ------------------------------------------DEVELOPER DATA ENTRY ----------------------------------------------------------------
def add_developer(dev_data):
    # If dev is STR or INT
    if str(dev_data['DevID']).isdigit() is True:
        dev_url = CG.DEVELOPER_BASE_URL_INT + str(dev_data['DevID'])
    else:
        dev_url = CG.DEVELOPER_BASE_URL_STRING + str(dev_data['DevID'])

    # --------------PREPARE DATA FOR PLLDEVLST --------------------
    PLLDEVLST_DATA =dict()
    PLLDEVLST_DATA['_id'] = dev_data['DevID']
    PLLDEVLST_DATA[DD.PLDEVLST_PLDDID_01] = dev_data['DevID']
    PLLDEVLST_DATA[DD.PLDEVLST_PLDNAM_02] = dev_data['DevName']
    PLLDEVLST_DATA[DD.PLDEVLST_PLDSTS_03] = True

    if AP.mongo_exists(DD.COLLECTION_00_PLDEVLST, dev_data['DevID']) is True:
        return "Developer:{0} Already exists in DB.".format(dev_data['DevID'])
    else:
        page = requests.get(dev_url)
        if page.status_code != 200:
            return "Invalid Developer ID"
        else:
            AP.mongo_write(DD.COLLECTION_00_PLDEVLST, dev_data['DevID'],PLLDEVLST_DATA)
            return "Developer:{0} Added to DB successfully.".format(dev_data['DevID'])
# ------------------------------------------KEYWORD DATA ENTRY ----------------------------------------------------------------
def add_keyword(kwd_data):
    query_url = CG.QUERY_BASE_HEAD + kwd_data['KwdID'] + CG.QUERY_BASE_TAIL

    # --------------PREPARE DATA FOR PLLKWDLST --------------------
    PLKWDLST_DATA =dict()
    PLKWDLST_DATA['_id'] = kwd_data['KwdID']
    PLKWDLST_DATA[DD.PLKWDLST_PLKKID_01] = kwd_data['KwdID']
    PLKWDLST_DATA[DD.PLKWDLST_PLKNAM_02] = kwd_data['KwdName']
    PLKWDLST_DATA[DD.PLKWDLST_PLKSTS_03] = True

    if AP.mongo_exists(DD.COLLECTION_00_PLKWDLST, kwd_data['KwdID']) is True:
        return "Keyword:{0} Already exists in DB.".format(kwd_data['KwdID'])
    else:
        page = requests.get(query_url)
        if page.status_code != 200:
            return "Invalid Keyword ID"
        else:
            AP.mongo_write(DD.COLLECTION_00_PLKWDLST, kwd_data['KwdID'], PLKWDLST_DATA)
            return "Keyword:{0} Added to DB successfully.".format(kwd_data['KwdID'])
