import os
import csv
import time
import json
import urllib
import urllib3
from urllib3 import *
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import ElementNotVisibleException,StaleElementReferenceException,NoSuchElementException
import APP_CONFIG
from APP_CONFIG import *


profile = webdriver.FirefoxProfile(FIREFOX_PROFILE)
profile.set_preference('browser.download.folderList', 2)
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', OUTPUT_DIR)
profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))
driver = webdriver.Firefox(profile)
wait = WebDriverWait(driver, 10)

BASE_URL = "http://gmail.com"
driver.get(BASE_URL)