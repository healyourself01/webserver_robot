# ------------------DATA BASE DESCRIPTIONS -------------------------------


# -------- PLDEVLST - Playstore Developer List ---- Begin--------------------
COLLECTION_00_PLDEVLST = "PLDEVLST"
# --------------------------------------------------------------------------
# Contains the initial links to crawl the playstore
# _ID = DEVELOPER
PLDEVLST_PLDDID_01 = "PLDDID" #Developer
PLDEVLST_PLDNAM_02 = "PLDNAM" #Developer
PLDEVLST_PLDSTS_03 = "PLDSTS" #Developer Status
# -------- PLDEVLST - Playstore Developer List ---- End--------------------

# -------- PLDEVLST - Playstore Developer List ---- Begin--------------------
COLLECTION_00_PLKWDLST = "PLKWDLST"
# --------------------------------------------------------------------------
# Contains the initial links to crawl the playstore
# _ID = DEVELOPER
PLKWDLST_PLKKID_01 = "PLKKID" #Developer
PLKWDLST_PLKNAM_02 = "PLKNAM" #Developer
PLKWDLST_PLKSTS_03 = "PLKSTS" #Developer Status
# -------- PLDEVLST - Playstore Developer List ---- End--------------------

# -------- PLINLNKS - Playstore Initial Links ---- Begin--------------------
COLLECTION_01_PLINLNKS = "PLINLNKS"
# --------------------------------------------------------------------------
# Contains the initial links to crawl the playstore
# _ID = DEVELOPER
PLINLNKS_PLLURL_01 = "PLLURL" #URL
PLINLNKS_PLLSRC_02 = "PLLSRC" #Source --  { {'source': 'developers'}, {'value':MindVisonApps} }
PLINLNKS_PLLR01LRDT_03 = "PLLR01LRDT" #Robot01 Last Run Date - {'2017-08-05'}
PLINLNKS_PLLR01STS_04 = "PLLR01STS" #Robot01 Status - {'2017-08-05': True}
PLINLNKS_PLLTOTAP_05 = "PLLTOTAP" #Total Apps
# -------- PLINLNKS - Playstore Initial Links ---- End--------------------

# ***************************************************************************************************
# -------- PLAPHDR - Playstore APP Header ---- Begin--------------------
COLLECTION_02_PLAPHDR = "PLAPHDR"
# --------------------------------------------------------------------------
# Contains the initial links to crawl the playstore
# _ID = PACKAGE
PLAPHDR_PLAHURL_01 = "PLAHURL" #URL
PLAPHDR_PLAHTTL_02 = "PLAHTTL"
PLAPHDR_PLAHDEV_02 = "PLAHDEV"
PLAPHDR_PLAHSRC_02 = "PLAHSRC"
PLINLNKS_PLLSRC_02 = "PLLSRC" #Source --  { {'source': 'developers'}, {'value':MindVisonApps} }
PLAPHDR_PLAHR02LRDT_03 = "PLAHR02LRDT" #Robot01 Last Run Date - {'2017-08-05'}
PLAPHDR_PLAHR02STS_04 = "PLAHR02STS" #Robot01 Status - {'2017-08-05': True}
# -------- PLAPHDR - Playstore APP Header---- End--------------------

# ***************************************************************************************************
# -------- PLAPDTL - Playstore APP Details ---- Begin--------------------
COLLECTION_03_PLAPDTL = "PLAPDTL"
# --------------------------------------------------------------------------
# Contains the initial links to crawl the playstore
# _ID = PACKAGE
PLAPDTL_PLADURL_01 = "PLADURL" #URL
PLAPDTL_PLADTTL_02 = "PLADTTL"
PLAPDTL_PLADSRC_02 = "PLADSRC"
PLINLNKS_PLLSRC_02 = "PLLSRC" #Source --  { {'source': 'developers'}, {'value':MindVisonApps} }
PLAPDTL_PLADR02LRDT_03 = "PLADR02LRDT" #Robot02 Last Run Date - {'2017-08-05'}
PLAPDTL_PLADR02STS_04 = "PLADR02STS" #Robot02 Status - {'2017-08-05': True}
PLAPDTL_PLADINST_05 = "PLADINST" #Number of Installs
PLAPDTL_PLADDEV_06 = "PLADDEV" #Developer
PLAPDTL_PLADDAT_06 = "PLADDAT" #Date
PLAPDTL_PLADVER_07 = "PLADVER" #Version
PLAPDTL_PLADEML_08 = "PLADEML" #Email
PLAPDTL_PLADRVW_09 = "PLADRVW" #Reviews
PLAPDTL_PLADSCR_10 = "PLADSCR" #Score
PLAPDTL_PLADRCHG_11 = "PLADRCHG" #Recent Change
PLAPDTL_PLADDSC_12 = "PLADDSC" #Description
PLAPDTL_PLADMNINS_13 = "PLADMNINS" #Min Installs
PLAPDTL_PLADMXINS_14 = "PLADMXINS" #Max Installs
PLAPDTL_PLADJULD_15 = "PLADJULD" #Julian Date
PLAPDTL_PLADINPD_16 = "PLADINPD" #Installs Per Day
PLAPDTL_PLADTDYS_17 = "PLADTDYS" # Total days passed since install
PLAPDTL_PLADTMNT_18 = "PLADTMNT" # Total Months passed since install
PLAPDTL_PLADTWKS_19 = "PLADTWKS" # Total Weeks passed since install


# -------- PLAPDTL - Playstore APP Details---- End--------------------
COLLECTION_04_PLAPDTLA01 ="PLAPDTLA01" #Analytics file - PLAPDTLA01



# ------------------DATA BASE DESCRIPTIONS -------------------------------